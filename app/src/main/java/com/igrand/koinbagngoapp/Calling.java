package com.igrand.koinbagngoapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Calling extends AppCompatActivity {

    private static final int REQUEST_PHONE_CALL = 1;
    Button calling;
    TextView number11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);

        final String number = getIntent().getStringExtra("Mobile");
        calling = findViewById(R.id.calling);
        number11=findViewById(R.id.number);

        number11.setText(number);
        calling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" +number));
                if (ActivityCompat.checkSelfPermission(Calling.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(Calling.this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                } else {

                    startActivity(intent);
                }


            }
        });
    }
}
