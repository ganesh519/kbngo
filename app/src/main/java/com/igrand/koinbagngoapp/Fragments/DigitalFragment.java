package com.igrand.koinbagngoapp.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.Adapters.RecyclerAdapter3;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class DigitalFragment extends Fragment {

    RecyclerView recyclerView2;
    ApiInterface apiInterface;
    AllRecyclerAdapter allRecyclerAdapter;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_digital, container, false);

        recyclerView2=(RecyclerView)v.findViewById(R.id.recyclerView3);




        final String id = getActivity().getIntent().getStringExtra("Id");
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse2> call = apiInterface.WalletResponse1(id);
        call.enqueue(new Callback<WalletResponse2>() {
            @Override
            public void onResponse(Call<WalletResponse2> call, Response<WalletResponse2> response) {
                if (response.isSuccessful());

                WalletResponse2 walletResponse1 = response.body();
                if (walletResponse1!=null) {


                    StatusDataResponse15 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();

                        // List<Data15> data15 = walletResponse1.data;

                        Data15 data15 = walletResponse1.data;

                        if (data15.all != null) {


                            List<All> all = data15.all;


                         /*   recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));
                            RecyclerAdapter3 adapter=new RecyclerAdapter3(getActivity());
                            recyclerView2.setAdapter(adapter);*/

                            recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));
                            allRecyclerAdapter = new AllRecyclerAdapter(getContext(), all);
                            recyclerView2.setAdapter(allRecyclerAdapter);


                        } else {
                            Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                        }
                    } else if (statusDataResponse7.code != 200) {

                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Internal Server Error", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WalletResponse2> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getActivity(), t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });




        return v;
    }
}