package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class WalletResponse001 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse001 status;
    @SerializedName("data")
    @Expose
    public List<Data001> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
