package com.igrand.koinbagngoapp.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.igrand.koinbagngoapp.Activities.AllRecyclerAdapter1;
import com.igrand.koinbagngoapp.Activities.Data14;
import com.igrand.koinbagngoapp.Activities.RequestRecyclerAdapter;
import com.igrand.koinbagngoapp.Activities.WalletResponse1;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.Data7;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;
import com.igrand.koinbagngoapp.Models.WalletResponse;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.Proceesing;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.Adapters.RecyclerAdapter;
import com.igrand.koinbagngoapp.Activities.Volunteers;
import com.igrand.koinbagngoapp.VolunteerRequests;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardFragment extends Fragment {
    PrefManager prefManager;
    String Id,f_name,mobile,userid,walletbals,photo;
    ApiInterface apiInterface;
    List<RecentTransaction> recentTransactions= new ArrayList<>();;
    RequestRecyclerAdapter requestRecyclerAdapter;
    RecyclerView recyclerView;
    NestedScrollView scroll;
    TextView requests;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);


        final DrawerLayout drawerLayout;


        prefManager=new PrefManager(getActivity());

        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        mobile=profile.get("Mobile");
        userid=profile.get("Userid");
        walletbals=profile.get("Walletbals");
        photo=profile.get("Photo");




        TextView ngoname1=(TextView) view.findViewById(R.id.ngoname1);
        ImageView profile1=(ImageView) view.findViewById(R.id.profile);
        final TextView n2890=(TextView) view.findViewById(R.id.n2890);
        final TextView n2891=(TextView) view.findViewById(R.id.n2891);
        final TextView n2892=(TextView) view.findViewById(R.id.n2892);
        final TextView moneyreq1=(TextView) view.findViewById(R.id.moneyreq);
        final TextView volunter1=(TextView) view.findViewById(R.id.volunter);
        final TextView requests=(TextView) view.findViewById(R.id.requests);
        final NestedScrollView scroll=(NestedScrollView) view.findViewById(R.id.scroll);
        final RecyclerView recyclerView=(RecyclerView) view.findViewById(R.id.recyclerView);


        if(photo!=null) {

            Picasso.get().load(photo).error(R.drawable.kb).into(profile1);

        } else {

            profile1.setImageResource(R.drawable.kb);
        }



        ngoname1.setText(f_name);



        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse9> call1 = apiInterface.Statuss9(Id);
        call1.enqueue(new Callback<StatusResponse9>() {
            @Override
            public void onResponse(Call<StatusResponse9> call, Response<StatusResponse9> response) {
                if (response.isSuccessful()) ;

                StatusResponse9 statusResponse = response.body();
                StatusDataResponse9 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog1.dismiss();


                   /* if(recentTransactions.isEmpty()) {

                       // Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();

                        requests.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);


                    } else {
*/
                        requests.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        recentTransactions = statusResponse.data.recentTransactions;
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        requestRecyclerAdapter = new RequestRecyclerAdapter(getContext(), recentTransactions);
                        recyclerView.setAdapter(requestRecyclerAdapter);
                  //  }



                    Data9 data = statusResponse.data;
                    String walletBal = data.walletBalance;
                    String transaction = data.totalTransaction;
                    String commission = data.commission;
                    String moneyreq = data.moneyRequests;
                    String volunteers = data.voulnteers;


                    n2890.setText(walletBal);
                    n2891.setText(transaction);
                    n2892.setText(commission);
                    moneyreq1.setText(moneyreq);
                    volunter1.setText(volunteers);


                } else if (statusDataResponse.code == 409) {
                    progressDialog1.dismiss();
                   // nested.setVisibility(View.GONE);
                    Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse9> call, Throwable t) {
               // nested.setVisibility(View.GONE);
                progressDialog1.dismiss();
//                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        CardView cardView=(CardView)view.findViewById(R.id.cardView);
        CardView requestcard=(CardView)view.findViewById(R.id.requestcard);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(), Volunteers.class);
                intent.putExtra("User_Id",userid);
                intent.putExtra("id",Id);
                intent.putExtra("Mobile_number",mobile);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });


        requestcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getActivity(), VolunteerRequests.class);
                intent.putExtra("User_Id",userid);
                intent.putExtra("id",Id);
                intent.putExtra("Mobile_number",mobile);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        ImageView toggle11=(ImageView)view.findViewById(R.id.toggle11);

        drawerLayout=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);

        toggle11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });


        return view;
    }

}

