package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data001 {

    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("requested_date")
    @Expose
    public String requestedDate;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("request_id")
    @Expose
    public String  request_id;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("amount", amount).append("requestedDate", requestedDate).append("updatedDate", updatedDate).append("status", status).append("request_id", request_id).toString();
    }
}
