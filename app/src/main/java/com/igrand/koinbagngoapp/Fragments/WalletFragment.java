package com.igrand.koinbagngoapp.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Activities.AllRecyclerAdapter1;
import com.igrand.koinbagngoapp.Activities.VolunteersDetails;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.Data7;
import com.igrand.koinbagngoapp.Models.WalletResponse;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.Adapters.RecyclerAdapter2;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class WalletFragment extends Fragment {
    ApiInterface apiInterface;
    //String Id;
    AllRecyclerAdapter0 allRecyclerAdapter1;


    RecyclerView recyclerView2;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wallet, container, false);


        final String id = getActivity().getIntent().getStringExtra("Id");

        recyclerView2=(RecyclerView)v.findViewById(R.id.recyclerView2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerAdapter2 adapter=new RecyclerAdapter2(getActivity());
        recyclerView2.setAdapter(adapter);


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse001> call = apiInterface.Statuss001(id);
        call.enqueue(new Callback<WalletResponse001>() {
            @Override
            public void onResponse(Call<WalletResponse001> call, Response<WalletResponse001> response) {
                 if (response.isSuccessful()) ;
                WalletResponse001 walletResponse1 = response.body();

                if (walletResponse1 != null) {

                    StatusDataResponse001 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();
                        recyclerView2.setVisibility(View.VISIBLE);



                        List<Data001> data7 = walletResponse1.data;


                        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));
                        allRecyclerAdapter1 = new AllRecyclerAdapter0(getContext(), data7);
                        recyclerView2.setAdapter(allRecyclerAdapter1);

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<WalletResponse001> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



        return v;
    }
}