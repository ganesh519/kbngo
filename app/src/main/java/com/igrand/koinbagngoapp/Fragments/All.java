package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class All {

    @SerializedName("api_type")
    @Expose
    public String apiType;
    @SerializedName("digital_number")
    @Expose
    public String digitalNumber;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("commission")
    @Expose
    public String commission;
    @SerializedName("prev_bal")
    @Expose
    public String prevBal;
    @SerializedName("cur_bal")
    @Expose
    public String curBal;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiType", apiType).append("digitalNumber", digitalNumber).append("amount", amount).append("commission", commission).append("prevBal", prevBal).append("curBal", curBal).append("status", status).append("createdAt", createdAt).toString();
    }
}
