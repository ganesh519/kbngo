package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class RecentTransaction {
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("middle_name")
    @Expose
    public String middleName;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("resquested_by")
    @Expose
    public String resquestedBy;
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("photo", photo).append("lastName", lastName).append("status", status).append("surname", surname).append("middleName", middleName).append("amount", amount).append("resquestedBy", resquestedBy).append("id", id).append("createdAt", createdAt).toString();
    }


}
