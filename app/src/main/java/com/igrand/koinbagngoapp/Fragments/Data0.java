package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data0 {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public Object middleName;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("wallet_balance")
    @Expose
    public String walletBalance;
    @SerializedName("commission")
    @Expose
    public String commission;
    @SerializedName("total_transaction")
    @Expose
    public String totalTransaction;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("middleName", middleName).append("surname", surname).append("mobileNumber", mobileNumber).append("photo", photo).append("walletBalance", walletBalance).append("commission", commission).append("totalTransaction", totalTransaction).toString();
    }
}
