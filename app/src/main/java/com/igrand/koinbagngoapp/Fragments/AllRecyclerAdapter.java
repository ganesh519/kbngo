package com.igrand.koinbagngoapp.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.R;

import java.util.List;

public class AllRecyclerAdapter extends RecyclerView.Adapter<AllRecyclerAdapter.Holder> {

    List<All> all;
    Context context;
    ApiInterface apiInterface;
    String id;


    public AllRecyclerAdapter(Context context, List<All> all) {

        this.context=context;
        this.all=all;
    }

    @NonNull
    @Override
    public AllRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview4, viewGroup, false);

        return new Holder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull AllRecyclerAdapter.Holder holder, int i) {



        String status1=all.get(i).status;

        if(status1.equals("Pending")) {

            holder.status.setText("Pending");
            holder.status.setBackgroundColor(Color.parseColor("#FFD71B"));
            holder.status.setTextColor(Color.parseColor("#000000"));
            holder.status.setBackgroundResource(R.drawable.textbackgroundyellow);
            holder.api_type.setText(all.get(i).apiType);
            holder.digitalnumber.setText(all.get(i).digitalNumber);
            holder.commissionlinear.setVisibility(View.GONE);
            holder.dcom1110.setVisibility(View.GONE);
            holder.prevbal.setText(all.get(i).prevBal);
            holder.curbal.setText(all.get(i).curBal);
            holder.createdat.setText(all.get(i).createdAt);
            holder.amount.setText(all.get(i).amount);
            //  holder.dcomrs1.setText(all.get(i).commission);

        }

        else if(status1.equals("Failed")){

            holder.status.setText("Failed");
            holder.status.setBackgroundColor(Color.parseColor("#FF0101"));
            holder.status.setTextColor(Color.parseColor("#FFFFFF"));
            holder.status.setBackgroundResource(R.drawable.textbackgroundred);
            holder.commissionlinear.setVisibility(View.GONE);
            holder.dcom1110.setVisibility(View.GONE);
            holder.api_type.setText(all.get(i).apiType);
            holder.digitalnumber.setText(all.get(i).digitalNumber);
            holder.prevbal.setText(all.get(i).prevBal);
            holder.curbal.setText(all.get(i).curBal);
            holder.createdat.setText(all.get(i).createdAt);
            holder.amount.setText(all.get(i).amount);
            holder.dcomrs1.setText(all.get(i).commission);



        }

        else if(status1.equals("Success")){

            holder.status.setText("Success");
            holder.status.setBackgroundColor(Color.parseColor("#00CB1F"));
            holder.status.setTextColor(Color.parseColor("#FFFFFF"));
            holder.commissionlinear.setVisibility(View.VISIBLE);
            holder.dcom1110.setVisibility(View.VISIBLE);
            holder.status.setBackgroundResource(R.drawable.textbackgroundgreen);
            holder.api_type.setText(all.get(i).apiType);
            holder.digitalnumber.setText(all.get(i).digitalNumber);
            holder.prevbal.setText(all.get(i).prevBal);
            holder.curbal.setText(all.get(i).curBal);
            holder.createdat.setText(all.get(i).createdAt);
            holder.amount.setText(all.get(i).amount);
            holder.dcomrs1.setText(all.get(i).commission);


        }



    }

    @Override
    public int getItemCount() {
        return all.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView api_type,digitalnumber,prevbal,curbal,createdat,status,amount,dcomrs1,dcom1110;
        LinearLayout linearcolor,commissionlinear;


        public Holder(@NonNull View itemView) {
            super(itemView);

            api_type=itemView.findViewById(R.id.api_type);
            digitalnumber=itemView.findViewById(R.id.digitalnumber);
            prevbal=itemView.findViewById(R.id.prevbal);
            curbal=itemView.findViewById(R.id.curbal);
            createdat=itemView.findViewById(R.id.created_at);
            status=itemView.findViewById(R.id.status);
            amount=itemView.findViewById(R.id.drs);
            dcomrs1=itemView.findViewById(R.id.dcomrs1);
            linearcolor=itemView.findViewById(R.id.linearcolor);
            commissionlinear=itemView.findViewById(R.id.commisionlinear);
            dcom1110=itemView.findViewById(R.id.dcom1110);


        }
    }
}
