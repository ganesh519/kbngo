package com.igrand.koinbagngoapp.Fragments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class Data9 {


    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public Object middleName;
    @SerializedName("surname")
    @Expose
    public Object surname;
    @SerializedName("last_name")
    @Expose
    public Object lastName;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("photo")
    @Expose
    public Object photo;
    @SerializedName("wallet_balance")
    @Expose
    public String walletBalance;
    @SerializedName("commission")
    @Expose
    public String commission;
    @SerializedName("total_transaction")
    @Expose
    public String totalTransaction;
    @SerializedName("money_requests")
    @Expose
    public String moneyRequests;
    @SerializedName("voulnteers")
    @Expose
    public String voulnteers;
    @SerializedName("recent_transactions")
    @Expose
    public List<RecentTransaction> recentTransactions = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("middleName", middleName).append("surname", surname).append("lastName", lastName).append("mobileNumber", mobileNumber).append("photo", photo).append("walletBalance", walletBalance).append("commission", commission).append("totalTransaction", totalTransaction).append("moneyRequests", moneyRequests).append("voulnteers", voulnteers).append("recentTransactions", recentTransactions).toString();
    }

}
