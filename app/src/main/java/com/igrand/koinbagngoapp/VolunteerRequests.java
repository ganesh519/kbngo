package com.igrand.koinbagngoapp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.igrand.koinbagngoapp.Activities.BaseActivity;
import com.igrand.koinbagngoapp.Activities.Data14;
import com.igrand.koinbagngoapp.Activities.RequestRecyclerAdapter1;
import com.igrand.koinbagngoapp.Activities.WalletResponse1;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VolunteerRequests extends BaseActivity {

    RecyclerView recycler_request;
    PrefManager prefManager;
    ApiInterface apiInterface;
    String Id;
    RequestRecyclerAdapter3 requestRecyclerAdapter3;
    ImageView backb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteer_requests);

        recycler_request = findViewById(R.id.recycler_request11);
        backb = findViewById(R.id.backb);

        backb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        prefManager = new PrefManager(VolunteerRequests.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        final ProgressDialog progressDialog = new ProgressDialog(VolunteerRequests.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse0> call = apiInterface.WalletResponse0(Id);
        call.enqueue(new Callback<WalletResponse0>() {
            @Override
            public void onResponse(Call<WalletResponse0> call, Response<WalletResponse0> response) {
                // if (response.isSuccessful()) ;


                WalletResponse0 walletResponse1 = response.body();

                if (walletResponse1!=null) {
                    StatusDataResponse000 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();
//                    Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                        List<Data000> data14List = walletResponse1.data;

                        if (walletResponse1.data != null) {

                           // proceesingList = data14List.proceesing;

                            recycler_request.setLayoutManager(new LinearLayoutManager(VolunteerRequests.this));
                            requestRecyclerAdapter3 = new RequestRecyclerAdapter3(VolunteerRequests.this, data14List);
                            recycler_request.setAdapter(requestRecyclerAdapter3);

                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(VolunteerRequests.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                        }


                    }else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(VolunteerRequests.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }

                }

                else {

                    progressDialog.dismiss();
                    Toast.makeText(VolunteerRequests.this, "Error", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WalletResponse0> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(VolunteerRequests.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
