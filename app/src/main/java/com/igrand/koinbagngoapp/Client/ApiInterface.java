package com.igrand.koinbagngoapp.Client;

import com.igrand.koinbagngoapp.Activities.AddVlist;
import com.igrand.koinbagngoapp.Activities.Approve;
import com.igrand.koinbagngoapp.Activities.SpinnerModel;
import com.igrand.koinbagngoapp.Activities.SpinnerModel1;
import com.igrand.koinbagngoapp.Activities.SpinnerModel2;
import com.igrand.koinbagngoapp.Activities.SpinnerModel3;
import com.igrand.koinbagngoapp.Activities.SpinnerModel4;
import com.igrand.koinbagngoapp.Activities.SpinnerModel5;
import com.igrand.koinbagngoapp.Activities.StatusResponse00;
import com.igrand.koinbagngoapp.Activities.StatusResponse10;
import com.igrand.koinbagngoapp.Activities.WalletResponse1;
import com.igrand.koinbagngoapp.Fragments.StatusResponse9;
import com.igrand.koinbagngoapp.Fragments.WalletRespons0;
import com.igrand.koinbagngoapp.Fragments.WalletResponse001;
import com.igrand.koinbagngoapp.Fragments.WalletResponse2;
import com.igrand.koinbagngoapp.Models.LoginResponse;
import com.igrand.koinbagngoapp.Models.LoginResponse1;
import com.igrand.koinbagngoapp.Models.LoginResponse2;
import com.igrand.koinbagngoapp.Models.StatusResponse4;
import com.igrand.koinbagngoapp.Models.StatusResponse5;
import com.igrand.koinbagngoapp.Models.StatusResponse8;
import com.igrand.koinbagngoapp.Models.WalletResponse;
import com.igrand.koinbagngoapp.StatusResponse12;
import com.igrand.koinbagngoapp.WalletResponse0;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-check-phone")
    Call<LoginResponse> Login(@Field("mobile_number") String mobile_number);

   @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-login")
    Call<LoginResponse1> Statuss1(@Field("mobile_number") String mobile_number, @Field("password") String password);

     @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-forgot-password")
    Call<LoginResponse2> Statuss2(@Field("mobile_number") String mobile_number);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-check-otp")
    Call<LoginResponse2> Statuss3(@Field("mobile_number") String mobile_number,@Field("otp") String otp);


  @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-reset-password")
    Call<StatusResponse4> Statuss4(@Field("mobile_number") String mobile_number, @Field("password") String password);


     @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-profile")
    Call<StatusResponse5> Statuss5(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-received")
    Call<WalletResponse> Statuss7(@Field("id") String id);

    @Multipart
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-request")
    Call<StatusResponse8> Statuss8(@Part MultipartBody.Part file,
                                   @Part("id") RequestBody id,
                                   @Part("amount") RequestBody amount,
                                   @Part("description") RequestBody description);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-login-withotp")
    Call<LoginResponse2> Statuss12(@Field("mobile_number") String mobile_number);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-status")
    Call<WalletResponse1> WalletResponse(@Field("id") String id);

  @FormUrlEncoded
  @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
  @POST("ngo-wallet-request-view-pending")
  Call<WalletResponse0> WalletResponse0(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("all-volunters-view")
    Call<WalletRespons0> Statuss0(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-request")
    Call<StatusResponse12> Statuss00(@Field("id") String id,
                                     @Field("amount") String amount,
                                     @Field("description") String description);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-details")
    Call<StatusResponse10> Statuss10(@Field("id") String id, @Field("request_id") String request_id);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-received")
    Call<StatusResponse10> Statuss11(@Field("id") String id, @Field("request_id") String request_id);



    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("ngo-home")
    Call<StatusResponse9> Statuss9(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunteer-profile")
    Call<StatusResponse00> VolunteerProfile(@Field("id") String id);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-digital-transaction-view")
    Call<WalletResponse2> WalletResponse1(@Field("id") String id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("user-wallet-transaction-view")
    Call<WalletResponse001> Statuss001(@Field("id") String id);


    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("volunters-create")
    Call<AddVlist> AddVList1(@Field("created_id") String created_id,@Field("title") String title,@Field("first_name") String first_name,@Field("surname") String surname,@Field("mobile_number") String mobile_number,@Field("date_of_birth") String date_of_birth,@Field("email") String email,
                            @Field("gender") String gender);


    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @GET("get-states")
    Call<SpinnerModel> spinner();

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("get-districts")
    Call<SpinnerModel1> spinner1(@Field("state_id") String state_id);

    @FormUrlEncoded
    @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
    @POST("get-revenue-divisions")
    Call<SpinnerModel2> spinner2(@Field("district_id") String district_id);

  @FormUrlEncoded
  @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
  @POST("get-mandals")
  Call<SpinnerModel3> spinner3(@Field("rev_id") String rev_id);

  @FormUrlEncoded
  @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
  @POST("get-villages")
  Call<SpinnerModel4> spinner4(@Field("mandal_id") String mandal_id);


  @FormUrlEncoded
  @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
  @POST("volunters-create")
  Call<SpinnerModel5> spinner5(@Field("state_id") String state_id,@Field("district_id") String district_id,@Field("rev_id") String rev_id,@Field("mandal_id") String mandal_id,@Field("village_id") String village_id,@Field("pincode") String pincode,@Field("addresslocation1") String addresslocation1,@Field("addresslocation2") String addresslocation2,@Field("addresslocation3") String addresslocation3);


  @FormUrlEncoded
  @Headers("koinbag:E40yth+CCJmYidP/kkxAx2tw400oIqbkAs/kJsd5EVE=")
  @POST("payment-accept-or-reject")
    Call<Approve> approve(@Field("id")String id,@Field("status") int status);

}

