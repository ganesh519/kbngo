package com.igrand.koinbagngoapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.igrand.koinbagngoapp.Activities.Approve;
import com.igrand.koinbagngoapp.Activities.Data10;
import com.igrand.koinbagngoapp.Activities.NavigationDrawerDashboard;
import com.igrand.koinbagngoapp.Activities.StatusDataResponse10;
import com.igrand.koinbagngoapp.Activities.StatusDataResponseapprove;
import com.igrand.koinbagngoapp.Activities.StatusResponse10;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.RecentTransaction;
import com.igrand.koinbagngoapp.Models.Data;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestRecyclerAdapter3 extends RecyclerView.Adapter<RequestRecyclerAdapter3.Holder> {

    List<Data000> proceesingList;
    Context context;
    PrefManager prefManager;
    String id;
    ApiInterface apiInterface;



    public RequestRecyclerAdapter3(Context context, List<Data000> proceesingList) {
        this.context = context;
        this.proceesingList = proceesingList;

    }


    @NonNull
    @Override
    public RequestRecyclerAdapter3.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listview1, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestRecyclerAdapter3.Holder holder, final int i) {

        holder.amount.setText(proceesingList.get(i).amount);
        holder.requested_date.setText(proceesingList.get(i).createdAt);

        String status1 = proceesingList.get(i).status;


        holder.amount.setText(proceesingList.get(i).amount);
        holder.requested_date.setText(proceesingList.get(i).createdAt);

        holder.i11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox3);
                dialog.show();
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                final TextView reqdate1, reqrs1, reqdes1, appdes, appdate;
                final ImageView image200;
                final RelativeLayout relative1;


                reqdate1 = dialog.findViewById(R.id.reqdate13);
                reqrs1 = dialog.findViewById(R.id.reqrs13);
                reqdes1 = dialog.findViewById(R.id.reqdes13);
                image200 = dialog.findViewById(R.id.image203);
                relative1 = dialog.findViewById(R.id.relative1);

                prefManager = new PrefManager(context);
                HashMap<String, String> profile = prefManager.getUserDetails();
                id = profile.get("id");
                String request_id = proceesingList.get(i).id;

                final ProgressDialog progressDialog1 = new ProgressDialog(context);
                progressDialog1.setMessage("Loading.....");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse10> call1 = apiInterface.Statuss10(id, request_id);
                call1.enqueue(new Callback<StatusResponse10>() {
                    @Override
                    public void onResponse(Call<StatusResponse10> call, Response<StatusResponse10> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse10 statusResponse = response.body();
                        if (statusResponse != null) {

                            StatusDataResponse10 statusDataResponse = statusResponse.status;

                            if (statusDataResponse.code == 200) {
                                progressDialog1.dismiss();
                                relative1.setVisibility(View.VISIBLE);

                                Data10 data = statusResponse.data;
                                String amount = data.amount;
                                String created_at = data.createdAt;
                                String updated_at = data.updatedAt;
                                String description = data.description;
                                String attachments = data.attachments;
                                String updateDescription = data.updateDescription;


                                reqdate1.setText(created_at);
                                reqrs1.setText(amount);
                                reqdes1.setText(description);
                                Picasso.get().load(attachments).error(R.drawable.kb).into(image200);
                                image200.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {


                                        image200.buildDrawingCache();
                                        Bitmap bitmap = image200.getDrawingCache();
                                        Intent intent = new Intent(context, ProfileImage.class);

                                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                        intent.putExtra("byteArray", _bs.toByteArray());
                                        context.startActivity(intent);

                                    }
                                });
                                // Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                            } else if (statusDataResponse.code == 409) {
                                progressDialog1.dismiss();
                                Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            }

                        }

                    }


                    @Override
                    public void onFailure(Call<StatusResponse10> call, Throwable t) {
                        progressDialog1.dismiss();
                        // Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast = Toast.makeText(context,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });


                ImageView close = (ImageView) dialog.findViewById(R.id.close);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

            }
        });


        holder.reqcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox4);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                TextView rejectbutton, approvebutton, volname, volname1, volid, volamount, rs1;
                final ImageView volpic;
                final LinearLayout dialog1;

                rejectbutton = dialog.findViewById(R.id.rejectbutton);
                approvebutton = dialog.findViewById(R.id.approvebutton);
                dialog1 = dialog.findViewById(R.id.dialog);
                volname = dialog.findViewById(R.id.volname);
                volid = dialog.findViewById(R.id.volid);
                volamount = dialog.findViewById(R.id.volamount);
                volpic = dialog.findViewById(R.id.volpic);
                rs1 = dialog.findViewById(R.id.rs1);
                volname1 = dialog.findViewById(R.id.volname1);

                String pic=proceesingList.get(i).photo;
                volname.setText(proceesingList.get(i).firstName);
                volname1.setText(proceesingList.get(i).surname);
                volid.setText(proceesingList.get(i).resquestedBy);
                volamount.setText(proceesingList.get(i).amount);
                rs1.setText(proceesingList.get(i).amount);
                Picasso.get().load(pic).error(R.drawable.kb).into(volpic);


                volpic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        volpic.buildDrawingCache();
                        Bitmap bitmap = volpic.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);
                    }
                });


                rejectbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int status = 0;
                        String id = proceesingList.get(i).id;


                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();
                       // dialog1.setVisibility(View.GONE);

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<Approve> call = apiInterface.approve(id, status);
                        call.enqueue(new Callback<Approve>() {
                            @Override
                            public void onResponse(Call<Approve> call, Response<Approve> response) {
                                if (response.isSuccessful()) ;

                                Approve walletResponse1 = response.body();
                                if (walletResponse1 != null) {
                                    StatusDataResponseapprove statusDataResponse7 = walletResponse1.status;
                                    if (statusDataResponse7.code == 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();
                                        Intent intent=new Intent(context, NavigationDrawerDashboard.class);
                                        context.startActivity(intent);
                                        //overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                                    } else if (statusDataResponse7.code != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                                    }

                                } else {

                                    progressDialog.dismiss();
                                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<Approve> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                                Toast toast = Toast.makeText(context,
                                        t.getMessage(), Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });


                    }
                });

                approvebutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        int status = 1;
                        String id = proceesingList.get(i).id;


                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<Approve> call = apiInterface.approve(id, status);
                        call.enqueue(new Callback<Approve>() {
                            @Override
                            public void onResponse(Call<Approve> call, Response<Approve> response) {
                                if (response.isSuccessful()) ;

                                Approve walletResponse1 = response.body();

                                if (walletResponse1 != null) {
                                    StatusDataResponseapprove statusDataResponse7 = walletResponse1.status;
                                    if (statusDataResponse7.code == 200) {
                                        progressDialog.dismiss();
                                        Intent intent=new Intent(context, NavigationDrawerDashboard.class);
                                        context.startActivity(intent);
                                        //context.notifyDataSetChanged();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();


                                    } else if (statusDataResponse7.code != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                                    }

                                } else {

                                    progressDialog.dismiss();
                                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<Approve> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                                Toast toast = Toast.makeText(context,
                                        t.getMessage(), Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();


                            }
                        });


                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return proceesingList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView requested_date, amount;
        ImageView i11;
        CardView reqcard;

        public Holder(@NonNull View itemView) {
            super(itemView);
            amount = itemView.findViewById(R.id.amount);
            requested_date = itemView.findViewById(R.id.requested_date);
            i11 = itemView.findViewById(R.id.i11);
            reqcard = itemView.findViewById(R.id.reqcard);

        }
    }
}
