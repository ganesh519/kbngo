package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import org.apache.commons.lang3.builder.ToStringBuilder;

public class StatusResponse2 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse2 status;
    @SerializedName("data")
    @Expose
    public Data2 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
