package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data2 {

    @SerializedName("otp")
    @Expose
    public Integer otp;


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("otp", otp).toString();
    }
}
