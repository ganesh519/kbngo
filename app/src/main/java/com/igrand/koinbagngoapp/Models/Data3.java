package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data3 {

    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("first_name")
    @Expose
    public String firstName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mobileNumber", mobileNumber).append("firstName", firstName).toString();
    }
}
