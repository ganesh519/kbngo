package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data1 {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("wallet_bal")
    @Expose
    public String walletBal;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("surname", surname).append("mobileNumber", mobileNumber).append("userId", userId).append("walletBal", walletBal).toString();
    }
}
