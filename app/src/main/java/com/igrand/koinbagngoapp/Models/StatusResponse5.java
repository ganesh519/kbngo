package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import org.apache.commons.lang3.builder.ToStringBuilder;

public class StatusResponse5 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse5 status;
    @SerializedName("data")
    @Expose
    public Data5 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
