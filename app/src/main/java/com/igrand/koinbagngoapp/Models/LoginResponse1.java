package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class LoginResponse1 {


    @SerializedName("status")
    private StatusBean1 status;
    @SerializedName("data")
    private DataBean1 data;

    public StatusBean1 getStatus() {
        return status;
    }

    public void setStatus(StatusBean1 status) {
        this.status = status;
    }

    public DataBean1 getData() {
        return data;
    }

    public void setData(DataBean1 data) {
        this.data = data;
    }


    public static class StatusBean1 {

        @SerializedName("code")
        private int code;
        @SerializedName("message")
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    public static class DataBean1 {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("surname")
        @Expose
        public String surname;
        @SerializedName("mobile_number")
        @Expose
        public String mobileNumber;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("wallet_bal")
        @Expose
        public String walletBal;
        @SerializedName("ngo_name")
        @Expose
        public String ngo_name;
        @SerializedName("photo")
        @Expose
        public String photo;




        @Override
        public String toString() {
            return new ToStringBuilder(this).append("id", id).append("firstName", firstName).append("surname", surname).append("mobileNumber", mobileNumber).append("userId", userId).append("walletBal", walletBal).toString();
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getWalletBal() {
            return walletBal;
        }

        public void setWalletBal(String walletBal) {
            this.walletBal = walletBal;
        }

        public String getNgo_name() {
            return ngo_name;
        }

        public void setNgo_name(String ngo_name) {
            this.ngo_name = ngo_name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }

}
