package com.igrand.koinbagngoapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data7 {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("attachments")
    @Expose
    public String attachments;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("amount", amount).append("createdAt", createdAt).append("description", description).append("status", status).append("attachments", attachments).toString();
    }
}
