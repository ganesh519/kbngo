package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class StatusResponse00 {


    @SerializedName("status")
    @Expose
    public StatusDataResponse00 status;
    @SerializedName("data")
    @Expose
    public Data00 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }

}
