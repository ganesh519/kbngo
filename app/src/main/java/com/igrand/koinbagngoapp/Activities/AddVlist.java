package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.igrand.koinbagngoapp.Fragments.Data0;
import com.igrand.koinbagngoapp.Fragments.StatusDataResponse0;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class AddVlist {

    @SerializedName("status")
    @Expose
    public StatusVlist status;
    @SerializedName("data")
    @Expose
    public DataVlist data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
