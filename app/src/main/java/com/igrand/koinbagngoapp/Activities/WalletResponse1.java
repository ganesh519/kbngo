package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class WalletResponse1 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse7 status;
    @SerializedName("data")
    @Expose
    public Data14 data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
