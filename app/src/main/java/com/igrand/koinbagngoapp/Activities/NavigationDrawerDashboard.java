package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.support.v4.widget.DrawerLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.DashboardFragment;
import com.igrand.koinbagngoapp.Models.Data5;
import com.igrand.koinbagngoapp.Models.StatusDataResponse5;
import com.igrand.koinbagngoapp.Models.StatusResponse5;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.ProfileImage;
import com.igrand.koinbagngoapp.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationDrawerDashboard extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {


    LinearLayout viewProfile, logout1;
    ImageView toggle, walletTransactions, digitalTransactions, home, profile;
    TextView volunteer, thome, tdigital, twallet, tprofile;
    Typeface typeface, typeface1, typeface2, typeface3, typeface4;
    NavigationView nav_view;
    BottomNavigationView bottomNavigationView;
    ApiInterface apiInterface;
    SharedPreferences sharedPreferences;
    Boolean exit = false;
    PrefManager prefManager;
    String Id, f_name, surname, mobile, userid, walletbals, Ngo_Name, Photo;
    public FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    final Fragment fragment1 = new DashboardFragment();
    final Fragment fragment2 = new MyWalletFragment();
    final Fragment fragment3 = new VolunteerListFragment();
    final Fragment fragment4 = new ProfileFragment();


    Fragment active =  fragment1;
    Fragment active2 = fragment2;
    Fragment active3 = fragment3;
    Fragment active4 = fragment4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer_dashboard);


        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new DashboardFragment(), "homee");
        fragmentTransaction.commit();

        prefManager = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        surname = profile.get("Surname");
        mobile = profile.get("Mobile");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");


        logout1 = findViewById(R.id.logout1);
        logout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefManager.clearSession();
                Intent intent = new Intent(NavigationDrawerDashboard.this, Login.class);
                intent.putExtra("Home", false);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            //applyFontToMenuItem(mi);
            applyFontToMenuItem(mi);
        }


        navigationView.getMenu().getItem(0).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(1).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(2).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_image);
        navigationView.getMenu().getItem(4).setActionView(R.layout.menu_image);


        View headerview = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        final TextView volunteer = (TextView) headerview.findViewById(R.id.volunteername1);
        final TextView volunteer2 = (TextView) headerview.findViewById(R.id.volunteername2);
        final TextView phnenum = (TextView) headerview.findViewById(R.id.phnenum);
        final TextView id1 = (TextView) headerview.findViewById(R.id.id1);
        LinearLayout viewprofile = (LinearLayout) headerview.findViewById(R.id.viewProfile);
        TextView wallet = (TextView) headerview.findViewById(R.id.wallet);
        final TextView rs = (TextView) headerview.findViewById(R.id.rs);
        TextView logout = (TextView) findViewById(R.id.logout);
        TextView volunteer11 = (TextView) findViewById(R.id.volunteername1);

        id1.setText(userid);
        volunteer.setText(f_name);
        phnenum.setText(mobile);
        //rs.setText(walletbals);
        volunteer2.setText("");


        LinearLayout viewProfile = (LinearLayout) headerview.findViewById(R.id.viewProfile);
        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog = new ProgressDialog(NavigationDrawerDashboard.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse5> call = apiInterface.Statuss5(Id);
                call.enqueue(new Callback<StatusResponse5>() {
                    @Override
                    public void onResponse(Call<StatusResponse5> call, Response<StatusResponse5> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse5 statusResponse = response.body();
                        StatusDataResponse5 statusDataResponse = statusResponse.status;

                        if (statusDataResponse.code == 200) {
                            progressDialog.dismiss();


                            Data5 data = statusResponse.data;

                            String title = data.title;
                            String surname = data.surname;
                            String last_name = data.lastName;
                            String middle_name = data.middleName;
                            String email = data.email;
                            String photo = data.photo;

                            String mobile_number = data.mobileNumber;
                            String date_of_birth = data.dateOfBirth;
                            String state = data.state;
                            String district = data.district;
                            String revenue_devision = data.revenueDevision;
                            String mandal = data.mandal;

                            String village = data.village;
                            String addressline1 = data.addressline1;
                            String addressline2 = data.addressline2;
                            String addressline3 = data.addressline3;
                            String pancard = data.pancard;
                            String panphoto = data.panphoto;

                            String aadharcard = data.aadharcard;
                            String aadharphoto = data.aadharphoto;
                            String _10th = data.tenthPhoto;
                            String inter = data.interPhoto;


                            // Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(NavigationDrawerDashboard.this, NGOProfile1.class);
                            intent.putExtra("Title", title);
                            intent.putExtra("SurName", surname);
                            intent.putExtra("LastName", last_name);
                            intent.putExtra("MiddleName", middle_name);
                            intent.putExtra("Email", email);
                            intent.putExtra("Photo", photo);
                            intent.putExtra("MobileNumber", mobile_number);
                            intent.putExtra("DateOfBirth", date_of_birth);
                            intent.putExtra("State", state);
                            intent.putExtra("District", district);
                            intent.putExtra("RevenueDevision", revenue_devision);
                            intent.putExtra("Mandal", mandal);
                            intent.putExtra("Village", village);
                            intent.putExtra("AddressLine1", addressline1);
                            intent.putExtra("AddressLine2", addressline2);
                            intent.putExtra("AddressLine3", addressline3);
                            intent.putExtra("Pancard", pancard);
                            intent.putExtra("PanPhoto", panphoto);
                            intent.putExtra("AadharCard", aadharcard);
                            intent.putExtra("AadharPhoto", aadharphoto);
                            intent.putExtra("Tenth", _10th);
                            intent.putExtra("Inter", inter);
                            intent.putExtra("UserId", userid);
                            intent.putExtra("FirstName", f_name);
                            startActivityForResult(intent, 100);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        } else if (statusDataResponse.code == 409) {
                            progressDialog.dismiss();
                            Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusResponse5> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(NavigationDrawerDashboard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


        updateNameTextView();

        bottomNavigationView = findViewById(R.id.the_bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }


    private void updateNameTextView() {

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerview = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        TextView volunteer = (TextView) headerview.findViewById(R.id.volunteername1);
        TextView volunteer2 = (TextView) headerview.findViewById(R.id.volunteername2);
        TextView phnenum = (TextView) headerview.findViewById(R.id.phnenum);
        TextView id1 = (TextView) headerview.findViewById(R.id.id1);
        //TextView viewprofile = (TextView) headerview.findViewById(R.id.viewProfile);
        TextView wallet = (TextView) headerview.findViewById(R.id.wallet);
        TextView rs = (TextView) headerview.findViewById(R.id.rs);
        TextView logout = (TextView) findViewById(R.id.logout);
        final ImageView imagekb = (ImageView) headerview.findViewById(R.id.imagekb);
        LinearLayout viewProfile = (LinearLayout) headerview.findViewById(R.id.viewProfile);

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        surname = profile.get("Surname");
        mobile = profile.get("Mobile");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");
        Ngo_Name = profile.get("NgoName");
        Photo = profile.get("Photo");


        id1.setText(userid);
        volunteer.setText(f_name);
        phnenum.setText(mobile);
        rs.setText(walletbals);
        volunteer2.setText(surname);
        if (Photo != null) {

            Picasso.get().load(Photo).error(R.drawable.kb).into(imagekb);

        } else {
            imagekb.setImageResource(R.drawable.kb);
        }


        imagekb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagekb.buildDrawingCache();
                Bitmap bitmap = imagekb.getDrawingCache();
                Intent intent = new Intent(NavigationDrawerDashboard.this, ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                startActivity(intent);
            }

        });


    }


    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new DashboardFragment(), "homee").hide(active);
                    fragmentTransaction.commit();
                    active = fragment1;
                    return true;


                case R.id.navigation_wallet:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new MyWalletFragment(), "").hide(active2);
                    fragmentTransaction.commit();
                    active2 = fragment2;
                    return true;

                case R.id.navigation_volunteer:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new VolunteerListFragment(), "").hide(active3);
                    fragmentTransaction.commit();
                    active3 = fragment3;

//                    fragmentManager.beginTransaction().hide(active).show(fragment3).commit();
//                    active = fragment3;
                    return true;

                case R.id.navigation_profile:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new ProfileFragment(), "").hide(active4);
                    fragmentTransaction.commit();
                    active4 = fragment4;
                    return true;
            }

            return false;
        }

    };

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (exit) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }

                this.exit = true;

                BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.the_bottom_navigation);
                bottomNavigationView.setSelectedItemId(R.id.navigation_home);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 5000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer_dashboard, menu);
        updateNameTextView();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        updateNameTextView();

        return super.onOptionsItemSelected(item);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "Avenir-Medium.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        if (item.getItemId() == R.id.nav_home) {

            getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new DashboardFragment()).commit();


        } else if (item.getItemId() == R.id.nav_digital) {

            // WalletTransactions.backbutton.setVisibility(VISIBLE);

            Intent intent = new Intent(NavigationDrawerDashboard.this, NGOWalletReq.class);

            intent.putExtra("User_Id", userid);
            intent.putExtra("FirstName", surname);
            startActivityForResult(intent, 100);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


            // getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new WalletTransactions1()).commit();


        } else if (item.getItemId() == R.id.nav_wallet) {


            Intent intent = new Intent(NavigationDrawerDashboard.this, MyWalletReq.class);
            startActivity(intent);


        } else if (item.getItemId() == R.id.nav_volunteers) {

            //getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new Commissions()).commit();
            Intent intent = new Intent(NavigationDrawerDashboard.this, Volunteers.class);
            startActivity(intent);


        } else if (item.getItemId() == R.id.nav_notifications) {

           /* Intent intent=new Intent(NavigationDrawerDashboard.this,Notifications.class);
            startActivity(intent);
*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        updateNameTextView();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case 100:
                if (resultCode == RESULT_OK) {
                    //Do action that's needed
                    break;
                }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
