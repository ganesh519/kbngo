package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.LoginResponse2;
import com.igrand.koinbagngoapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP extends BaseActivity {

    EditText editText1,editText2,editText3,editText4,editText5,editText6;
    Button proceed3;
    ApiInterface apiInterface;
    LinearLayout otp1;
    String text;
    boolean delete=false;
    ImageView back1122;
    TextView resendotp1,name002;
    LoginResponse2.StatusBean2 statusBean1;
    private boolean isPaused = false;
    private boolean isCanceled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        editText1=findViewById(R.id.editText1);

        back1122=findViewById(R.id.backpage11212);
        resendotp1=findViewById(R.id.resendotp1);
        name002=findViewById(R.id.name002);

        final String mobile_number=getIntent().getStringExtra("Mobile_number");
        final String name=getIntent().getStringExtra("Name1");
        final String surName1=getIntent().getStringExtra("SurName1");

        name002.setText(name);


        resendotp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                new CountDownTimer(30000, 1000) {


                    public void onTick(long millisUntilFinished) {

                        resendotp1.setText("Resend OTP: " + millisUntilFinished / 1000);
                        resendotp1.setEnabled(false);

                    }

                    public void onFinish() {
                        resendotp1.setText("Re-send OTP?");
                        resendotp1.setEnabled(true);
                    }



                }.start();




                final String mobile_number = getIntent().getStringExtra("Mobile_number");

                final ProgressDialog progressDialog = new ProgressDialog(OTP.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<LoginResponse2> call = apiInterface.Statuss2(mobile_number);
                call.enqueue(new Callback<LoginResponse2>() {
                    @Override
                    public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {





                        if (response.code() == 200) {

                            progressDialog.dismiss();
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            LoginResponse2.DataBean2 dataBean = response.body().getData();
                            //Integer otp = dataBean.otp;

                            //Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();
                            /*Intent intent = new Intent(OTP.this, OTP1.class);
                            intent.putExtra("Mobile_number", mobile_number);
                            intent.putExtra("OTP", otp);
                            startActivity(intent);*/

                        }else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                        }

                    }



                    @Override
                    public void onFailure(Call<LoginResponse2> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(OTP.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });



            }
        });


        proceed3=findViewById(R.id.proceed3);
        proceed3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String mobile_number=getIntent().getStringExtra("Mobile_number");
                String otp = editText1.getText().toString();
                if (!otp.isEmpty()) {
                    // String otp=getIntent().getStringExtra("OTP");

                    final ProgressDialog progressDialog = new ProgressDialog(OTP.this);
                    progressDialog.setMessage("Verifying Details.....");
                    progressDialog.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse2> call = apiInterface.Statuss3(mobile_number, otp);
                    call.enqueue(new Callback<LoginResponse2>() {
                        @Override
                        public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                //Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();


                                LoginResponse2.DataBean2 dataBean = response.body().getData();

                                Intent intent = new Intent(OTP.this, SetPin.class);
                                intent.putExtra("Mobile_number", mobile_number);
                                intent.putExtra("Name1",name);
                                intent.putExtra("SurName1",surName1);
                                startActivity(intent);


                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<LoginResponse2> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }

                else {
                    Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_LONG).show();
                }

            }


        });

        back1122.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

}


