package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Approve {

    @SerializedName("status")
    @Expose
    public StatusDataResponseapprove status;
    @SerializedName("data")
    @Expose
    public Dataapprove data;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
