package com.igrand.koinbagngoapp.Activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;

public class MainActivity extends BaseActivity {

    private static int splashscreentimeout=2000;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefManager=new PrefManager(MainActivity.this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (prefManager.isLoggedIn()){

                    Intent intent = new Intent(MainActivity.this, NavigationDrawerDashboard.class);
                    startActivity(intent);

                }
                else {
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    startActivity(intent);
                }


                }
        },splashscreentimeout);

    }

    protected void onLeaveThisActivity() {
    }

    protected void onStartNewActivity() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


    }
}
