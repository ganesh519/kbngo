package com.igrand.koinbagngoapp.Activities;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Adapters.MyAdapter;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.DigitalFragment;
import com.igrand.koinbagngoapp.Fragments.WalletFragment;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.VolunteersDetails1;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VolunteersDetails extends BaseActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    LinearLayout calllinear,viewMore;
    ImageView back77,pic11;
    ApiInterface apiInterface;
    TextView name,userid,emailid,n2890,n2891,n2892,namel;
    LinearLayout vollinear;
    ViewPagerAdapterNotifications pagerAdapterNotifications;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteers_details);

        tabLayout = findViewById(R.id.tabLayout0);
        viewPager = findViewById(R.id.viewPager0);
        calllinear=findViewById(R.id.calllinear);
        viewMore=findViewById(R.id.viewMore);
        back77=findViewById(R.id.back77);
        name=findViewById(R.id.name);
        userid=findViewById(R.id.userid);
        emailid=findViewById(R.id.emailid);
        n2890=findViewById(R.id.n2890);
        n2891=findViewById(R.id.n2891);
        n2892=findViewById(R.id.n2892);
        pic11=findViewById(R.id.photo);
        vollinear=findViewById(R.id.vollinear);
        namel=findViewById(R.id.namel);


        final String id = getIntent().getStringExtra("Id");
        final String wallet = getIntent().getStringExtra("Wallet");
        final String trans = getIntent().getStringExtra("Trans");
        final String comm = getIntent().getStringExtra("Comm");
        final String pic = getIntent().getStringExtra("Pic");
        final String number = getIntent().getStringExtra("Num");
        final String firstname = getIntent().getStringExtra("Name");



        final ProgressDialog progressDialog = new ProgressDialog(VolunteersDetails.this);
        progressDialog.setMessage("Verifying Details.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse00> call = apiInterface.VolunteerProfile(id);
        call.enqueue(new Callback<StatusResponse00>() {
            @Override
            public void onResponse(Call<StatusResponse00> call, Response<StatusResponse00> response) {
                if (response.isSuccessful()) ;

                StatusResponse00 statusResponse = response.body();
                StatusDataResponse00 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog.dismiss();

                    vollinear.setVisibility(View.VISIBLE);

                    Data00 data = statusResponse.data;

                    final String title = data.title;
                    final String surname = data.surname;
                    final String last_name = data.lastName;
                    final String middle_name = data.middleName;
                    final String email = data.email;
                    final String photo = data.photo;
                    final String mobile_number = data.mobileNumber;
                    final String date_of_birth = data.dateOfBirth;
                    final String state = data.state;
                    final String district = data.district;
                    final String revenue_devision = data.revenueDevision;
                    final String mandal = data.mandal;
                    final String village = data.village;
                    final String addressline1 = data.addressline1;
                    final String addressline2 = data.addressline2;
                    final String addressline3 = data.addressline3;
                    final String pancard = data.pancard;
                    final String panphoto = data.panphoto;
                    final String aadharcard = data.aadharcard;
                    final String aadharphoto = data.aadharphoto;
                    final String _10th = data.tenthPhoto;
                    final String inter = data.interPhoto;
                    final String firstname=data.firstName;
                    final String tenthmarks=data.tenthMarks;
                    final String intermarks=data.interMarks;
                    final String degreemarks=data.degreeMarks;
                    final String contactname=data.contactName;
                    final String contacttype=data.contactType;
                    final String contactnumber=data.contactNumber;
                    final String relation=data.relation;


                    name.setText(firstname);
                    n2890.setText(wallet);
                    n2891.setText(trans);
                    n2892.setText(comm);
                    namel.setText(surname);
                    Picasso.get().load(pic).into(pic11);




                    viewMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //Toast.makeText(NavigationDrawerDashboard.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(VolunteersDetails.this, VolunteersDetails1.class);
                            intent.putExtra("Title", title);
                            intent.putExtra("SurName", surname);
                            intent.putExtra("LastName", last_name);
                            intent.putExtra("MiddleName", middle_name);
                            intent.putExtra("Email", email);
                            intent.putExtra("Photo", photo);
                            intent.putExtra("MobileNumber", mobile_number);
                            intent.putExtra("DateOfBirth", date_of_birth);
                            intent.putExtra("State", state);
                            intent.putExtra("District", district);
                            intent.putExtra("RevenueDevision", revenue_devision);
                            intent.putExtra("Mandal", mandal);
                            intent.putExtra("Village", village);
                            intent.putExtra("AddressLine1", addressline1);
                            intent.putExtra("AddressLine2", addressline2);
                            intent.putExtra("AddressLine3", addressline3);
                            intent.putExtra("Pancard", pancard);
                            intent.putExtra("PanPhoto", panphoto);
                            intent.putExtra("AadharCard", aadharcard);
                            intent.putExtra("AadharPhoto", aadharphoto);
                            intent.putExtra("Tenth", _10th);
                            intent.putExtra("Inter", inter);
                            //intent.putExtra("UserId", userid);
                            //intent.putExtra("FirstName", first_name);

                            intent.putExtra("FirstName", firstname);
                            intent.putExtra("TenthMarks", tenthmarks);
                            intent.putExtra("InterMarks", intermarks);
                            intent.putExtra("DegreeMarks", degreemarks);
                            intent.putExtra("ContactName", contactname);
                            intent.putExtra("ContactType", contacttype);
                            intent.putExtra("ContactNumber", contactnumber);
                            intent.putExtra("Relation", relation);
                            intent.putExtra("Pic",photo);

                            startActivityForResult(intent, 100);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        }
                    });





                } else if (statusDataResponse.code == 409) {
                    progressDialog.dismiss();
                    Toast.makeText(VolunteersDetails.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse00> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(NavigationDrawerDashboard.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(VolunteersDetails.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });













        back77.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(VolunteersDetails.this,Volunteers.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/
               finish();
            }
        });
        /*viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VolunteersDetails.this,NGOProfile1.class);
                startActivity(intent);
            }
        });*/
        calllinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(VolunteersDetails.this);
                dialog.setContentView(R.layout.dialogbox7);
                dialog.show();
                /*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*/
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                TextView callname,callnumber;
                ImageView callpic;
                Button callbutton;

                callname = dialog.findViewById(R.id.callname);
                callnumber = dialog.findViewById(R.id.callnumber);
                callpic = dialog.findViewById(R.id.callpic);
                callbutton = dialog.findViewById(R.id.callbutton);


                callname.setText(firstname);
                callnumber.setText(number);
                Picasso.get().load(pic).into(callpic);


                callbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" +number));
                        if (ActivityCompat.checkSelfPermission(VolunteersDetails.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            // ActivityCompat.requestPermissions(Context, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        } else {

                            startActivity(intent);
                        }

                    }
                });


            }
        });

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabLayout.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFE469"));




        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        pagerAdapterNotifications.addFragment(new DigitalFragment(), "Digital Transactions");
        pagerAdapterNotifications.addFragment(new WalletFragment(), "Wallet Transactions");

        viewPager.setAdapter(pagerAdapterNotifications);
        tabLayout.setupWithViewPager(viewPager);



    }



    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }















       /* tabLayout.addTab(tabLayout.newTab().setText("Digital Transactions"));
        tabLayout.addTab(tabLayout.newTab().setText("Wallet Transactions"));


        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());


        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/
    }
}

