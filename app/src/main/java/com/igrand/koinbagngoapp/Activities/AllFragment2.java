package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.Data7;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;
import com.igrand.koinbagngoapp.Models.WalletResponse;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllFragment2 extends Fragment {
    ApiInterface apiInterface;
    RecyclerView recycler_allview;
    PrefManager prefManager;
    String Id;
    AllRecyclerAdapter1 allRecyclerAdapter1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_all, container, false);

        recycler_allview=v.findViewById(R.id.recycler_allview);
        /*recycler_reject=v.findViewById(R.id.recycler_reject);
        recycler_approved=v.findViewById(R.id.recycler_approved);
*/
        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse> call = apiInterface.Statuss7(Id);
        call.enqueue(new Callback<WalletResponse>() {
            @Override
            public void onResponse(Call<WalletResponse> call, Response<WalletResponse> response) {
                // if (response.isSuccessful()) ;
                WalletResponse walletResponse1 = response.body();
                StatusDataResponse7 statusDataResponse7 = walletResponse1.status;
                if (statusDataResponse7.code == 200) {
                    progressDialog.dismiss();

                    List<Data7> data7 = walletResponse1.data;


                    recycler_allview.setLayoutManager(new LinearLayoutManager(getActivity()));
                    allRecyclerAdapter1 = new AllRecyclerAdapter1(getContext(), data7);
                    recycler_allview.setAdapter(allRecyclerAdapter1);



                } else if (statusDataResponse7.code != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WalletResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



        return v;
    }
}
