package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.LoginResponse2;
import com.igrand.koinbagngoapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTP2 extends BaseActivity {

    EditText editText1, editText2, editText3, editText4, editText5, editText6;
    Button proceed3;
    TextView name1102;
    LinearLayout otp1;
    String text;
    boolean delete = false;
    ImageView backpage20;
    ApiInterface apiInterface;
    LoginResponse2.StatusBean2 statusBean1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp2);

        proceed3 = findViewById(R.id.proceed3);
        name1102 = findViewById(R.id.name1102);
        backpage20 = findViewById(R.id.backpage20);
        final String mobileNumber = getIntent().getStringExtra("MobileNumber");
        final String firstName = getIntent().getStringExtra("FirstName");
        editText1 = findViewById(R.id.editText1);

        name1102.setText(firstName);


        backpage20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final ProgressDialog progressDialog = new ProgressDialog(OTP2.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse2> call = apiInterface.Statuss12(mobileNumber);
        call.enqueue(new Callback<LoginResponse2>() {
            @Override
            public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                if (response.code() == 200) {

                    progressDialog.dismiss();
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    LoginResponse2.DataBean2 dataBean = response.body().getData();


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity2.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(OTP2.this,
                            "Please Check the OTP", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();
                    finish();

                }

            }


            @Override
            public void onFailure(Call<LoginResponse2> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity2.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                finish();
                Toast toast = Toast.makeText(OTP2.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        proceed3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final String mobile_number = getIntent().getStringExtra("Mobile_number");
                String otp = editText1.getText().toString();
                if (!otp.isEmpty()) {
                    // String otp=getIntent().getStringExtra("OTP");

                    final ProgressDialog progressDialog = new ProgressDialog(OTP2.this);
                    progressDialog.setMessage("Verifying Details.....");
                    progressDialog.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse2> call = apiInterface.Statuss3(mobile_number, otp);
                    call.enqueue(new Callback<LoginResponse2>() {
                        @Override
                        public void onResponse(Call<LoginResponse2> call, Response<LoginResponse2> response) {


                            if (response.code() == 200) {

                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                //Toast.makeText(OTP.this, statusBean1.getMessage(), Toast.LENGTH_SHORT).show();


                                LoginResponse2.DataBean2 dataBean = response.body().getData();

                                Intent intent = new Intent(OTP2.this, AddVolunteerDetails.class);
                               startActivity(intent);


                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(OTP2.this, "Please Check the OTP", Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<LoginResponse2> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(OTP2.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter OTP", Toast.LENGTH_LONG).show();
                }

            }

        });
    }
}





