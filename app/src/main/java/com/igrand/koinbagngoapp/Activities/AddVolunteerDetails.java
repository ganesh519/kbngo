package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVolunteerDetails extends BaseActivity {

    Button next01;
    ImageView back18;
    EditText pincode1, al1, al2, al3;
    private List<SpinnerModel> goodModelArrayList;
    ApiInterface apiInterface;
    AllRecyclerAdapter1 allRecyclerAdapter1;
    ArrayList<String> States;
    Spinner states, districts, revenuedivisions, mandal, village;
    String state_id, district_id, rev_id,mandal_id, village_id,vid,
            state,district,revdiv,mand,vill;
    ArrayAdapter<String> adapterstates;
    ArrayAdapter<String> adapterdistricts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_volunteer_details);

        next01 = findViewById(R.id.next01);
        back18 = findViewById(R.id.back18);
        states = findViewById(R.id.states);
        districts = findViewById(R.id.districts);
        revenuedivisions = findViewById(R.id.revenuedivisions);
        mandal = findViewById(R.id.mandal);
        village = findViewById(R.id.village);
        pincode1 = findViewById(R.id.pincode);
        al1 = findViewById(R.id.al1);
        al2 = findViewById(R.id.al2);
        al3 = findViewById(R.id.al3);


        back18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(AddVolunteerDetails.this,OTP2.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/
                finish();
            }
        });


        final ProgressDialog progressDialog4 = new ProgressDialog(AddVolunteerDetails.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel> call3 = apiInterface.spinner();
        call3.enqueue(new Callback<SpinnerModel>() {
            @Override
            public void onResponse(Call<SpinnerModel> call, Response<SpinnerModel> response) {
                if (response.isSuccessful()) ;

                SpinnerModel statusResponse = response.body();
                StatusSpinner statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {

                    progressDialog4.dismiss();


                    final List<Category_spinner> state_spinner = new ArrayList<>();
                    List<DataSpinner> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(data.get(i).stateName);
                        state1.setId(data.get(i).id);
                        state_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, state_spinner);
                        states.setAdapter(customSpinnerAdapter1);
                        states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                state_id = state_spinner.get(position).getId();
                                Dist(state_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }






                } else if (statusDataResponse.code == 409) {
                    progressDialog4.dismiss();

                    Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpinnerModel> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });
    }

    private void Dist(String state_id) {

        final ProgressDialog progressDialog4 = new ProgressDialog(AddVolunteerDetails.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel1> call4 = apiInterface.spinner1(state_id);
        call4.enqueue(new Callback<SpinnerModel1>() {
            @Override
            public void onResponse(Call<SpinnerModel1> call, Response<SpinnerModel1> response) {
                if (response.isSuccessful()) ;

                SpinnerModel1 statusResponse = response.body();
                StatusSpinner1 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog4.dismiss();
                    final List<Category_spinner> city_spinner = new ArrayList<>();
                    List<DataSpinner1> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(data.get(i).districtName);
                        state1.setId(String.valueOf(data.get(i).id));
                        city_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, city_spinner);
                        districts.setAdapter(customSpinnerAdapter1);
                        districts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                district_id = city_spinner.get(position).getId();
                                RevDiv(district_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                } else if (statusDataResponse.code == 409) {
                    progressDialog4.dismiss();

                    Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpinnerModel1> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


    }

    private void RevDiv(String district_id) {

        final ProgressDialog progressDialog4 = new ProgressDialog(AddVolunteerDetails.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel2> call4 = apiInterface.spinner2(district_id);
        call4.enqueue(new Callback<SpinnerModel2>() {
            @Override
            public void onResponse(Call<SpinnerModel2> call, Response<SpinnerModel2> response) {
                if (response.isSuccessful()) ;

                SpinnerModel2 statusResponse = response.body();
                StatusSpinner2 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog4.dismiss();
                    final List<Category_spinner> city_spinner = new ArrayList<>();
                    List<DataSpinner2> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(data.get(i).revenueDivisionName);
                        state1.setId(String.valueOf(data.get(i).id));
                        city_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, city_spinner);
                        revenuedivisions.setAdapter(customSpinnerAdapter1);
                        revenuedivisions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                rev_id = city_spinner.get(position).getId();
                                Mandal(rev_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                } else if (statusDataResponse.code == 409) {
                    progressDialog4.dismiss();

                    Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpinnerModel2> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });




    }

    private void Mandal(String rev_id) {


        final ProgressDialog progressDialog4 = new ProgressDialog(AddVolunteerDetails.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel3> call4 = apiInterface.spinner3(rev_id);
        call4.enqueue(new Callback<SpinnerModel3>() {
            @Override
            public void onResponse(Call<SpinnerModel3> call, Response<SpinnerModel3> response) {
                if (response.isSuccessful()) ;

                SpinnerModel3 statusResponse = response.body();
                StatusSpinner3 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog4.dismiss();
                    final List<Category_spinner> city_spinner = new ArrayList<>();
                    List<DataSpinner3> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(data.get(i).mandalName);
                        state1.setId(String.valueOf(data.get(i).id));
                        city_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, city_spinner);
                        mandal.setAdapter(customSpinnerAdapter1);
                        mandal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                mandal_id = city_spinner.get(position).getId();
                                Village(mandal_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                } else if (statusDataResponse.code == 409) {
                    progressDialog4.dismiss();

                    Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpinnerModel3> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

    }

    private void Village(String mandal_id) {

        final ProgressDialog progressDialog4 = new ProgressDialog(AddVolunteerDetails.this);
        progressDialog4.setMessage("Loading.....");
        progressDialog4.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SpinnerModel4> call4 = apiInterface.spinner4(mandal_id);
        call4.enqueue(new Callback<SpinnerModel4>() {
            @Override
            public void onResponse(Call<SpinnerModel4> call, Response<SpinnerModel4> response) {
                if (response.isSuccessful()) ;

                SpinnerModel4 statusResponse = response.body();
                StatusSpinner4 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog4.dismiss();
                    final List<Category_spinner> city_spinner = new ArrayList<>();
                    List<DataSpinner4> data = statusResponse.data;
                    for (int i = 0; i < data.size(); i++) {
                        final Category_spinner state1 = new Category_spinner();
                        state1.setQuantiti(data.get(i).villageName);
                        state1.setId(String.valueOf(data.get(i).id));
                        city_spinner.add(state1);

                        CustomSpinnerAdapter customSpinnerAdapter1 = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, city_spinner);
                        mandal.setAdapter(customSpinnerAdapter1);
                        mandal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                vid = city_spinner.get(position).getId();
                                //Village(village_id);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }
                } else if (statusDataResponse.code == 409) {

                    Toast.makeText(getApplicationContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SpinnerModel4> call, Throwable t) {

                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(getApplicationContext(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

    }
}

