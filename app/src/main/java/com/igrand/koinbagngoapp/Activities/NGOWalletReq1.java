package com.igrand.koinbagngoapp.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.Data8;
import com.igrand.koinbagngoapp.Models.StatusDataResponse8;
import com.igrand.koinbagngoapp.Models.StatusResponse8;
import com.igrand.koinbagngoapp.R;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NGOWalletReq1 extends BaseActivity {

    public static final int REQUEST_CODE = 1;
    Button submit,ok;
    LinearLayout upload;
    EditText amount1,description1;
    ApiInterface apiInterface;
    ImageView back001;

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED)  {
                Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngowallet_req1);

        final SharedPreferences sharedPreferences=getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        final String id=sharedPreferences.getString("ID","");


        amount1=findViewById(R.id.amount2);
        description1=findViewById(R.id.description2);


        submit=findViewById(R.id.submit1);
        back001=findViewById(R.id.back001);
        back001.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NGOWalletReq1.this,NGOWalletReq.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*Intent intent=new Intent(WalletRequest.this,WalletTransactions.class);
                startActivity(intent);*//*



                final String amount=amount1.getText().toString();
                String description=description1.getText().toString();


                final ProgressDialog progressDialog=new ProgressDialog(NGOWalletReq1.this);
                progressDialog.setMessage("Verifying Details.....");
                progressDialog.show();

                apiInterface= ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse8> call=apiInterface.Statuss8(id,amount,description);
                call.enqueue(new Callback<StatusResponse8>() {
                    @Override
                    public void onResponse(Call<StatusResponse8> call, Response<StatusResponse8> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse8 statusResponse = response.body();
                        StatusDataResponse8 statusDataResponse=statusResponse.status;

                        if (statusDataResponse.code == 200){
                            progressDialog.dismiss();


                            Data8 data=statusResponse.data;

                            // String name=data.firstName;

                            Toast.makeText(NGOWalletReq1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                            final Dialog dialog = new Dialog(NGOWalletReq1.this);
                            dialog.setContentView(R.layout.dialogbox3);
                            dialog.show();



                            TextView walletrs,walletrs1;
                            walletrs=dialog.findViewById(R.id.walletrs);
                            walletrs1=dialog.findViewById(R.id.walletrs1);
                            walletrs1.setText(amount);
                            walletrs.setText(amount);


                *//*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*//*
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                            Button ok=(Button)dialog.findViewById(R.id.ok);
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent=new Intent(NGOWalletReq1.this,NGOWalletReq.class);
                                    startActivity(intent);

                                    // dialog.dismiss();
                                }
                            });
                        }
                        else if (statusDataResponse.code == 409 ){
                            progressDialog.dismiss();
                            Toast.makeText(NGOWalletReq1.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<StatusResponse8> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(NGOWalletReq1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            }

        });





                *//*final Dialog dialog = new Dialog(NGOWalletReq1.this);
                dialog.setContentView(R.layout.dialogbox3);
                dialog.show();
                *//**//*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*//**//*
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                Button ok=(Button)dialog.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(NGOWalletReq1.this,NGOWalletReq.class);
                        startActivity(intent);

                        // dialog.dismiss();
                    }
                });*//*



        upload=findViewById(R.id.upload1);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "content://media/internal/images/media"));
                startActivity(intent);*//*


                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE);
            }

        });



    }*/
}
