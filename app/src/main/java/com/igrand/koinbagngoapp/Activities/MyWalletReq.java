package com.igrand.koinbagngoapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagngoapp.Adapters.MyAdapter1;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.WalletRequest;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyWalletReq extends BaseActivity {

    TabLayout tabs_wallet;
    ViewPager viewPager;
    ImageView back,walletRequest;
    ImageView toggledigital;
    DrawerLayout drawerLayout;
    TextView textview,textview1;
    Typeface typeface;
    ViewPagerAdapterNotifications pagerAdapterNotifications;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteer_wallet_req);

        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();

                Intent intent=new Intent(MyWalletReq.this, NavigationDrawerDashboard.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
        tabs_wallet = (TabLayout)findViewById(R.id.tabs_wallet);
        viewPager = (ViewPager)findViewById(R.id.viewPager);

        tabs_wallet.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabs_wallet.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));


        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getSupportFragmentManager());
        pagerAdapterNotifications.addFragment(new AllFragment3(), "All");
        pagerAdapterNotifications.addFragment(new RequestsFragment2(), "Requests");
        pagerAdapterNotifications.addFragment(new ApprovedFragment2(), "Approved");
        pagerAdapterNotifications.addFragment(new RejectedFragment2(), "Rejected");

        viewPager.setAdapter(pagerAdapterNotifications);
        tabs_wallet.setupWithViewPager(viewPager);

        walletRequest=findViewById(R.id.walletRequest1);
        walletRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), WalletRequest.class);
                startActivity(intent);
            }
        });

    }



    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }

}
