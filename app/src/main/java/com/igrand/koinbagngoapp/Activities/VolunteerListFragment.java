package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.igrand.koinbagngoapp.Adapters.RecyclerAdapter1;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.Data0;
import com.igrand.koinbagngoapp.Fragments.StatusDataResponse0;
import com.igrand.koinbagngoapp.Fragments.WalletRespons0;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VolunteerListFragment extends Fragment {

    PrefManager prefManager;
    String Id,f_name,surname,mobile,userid,walletbals,Ngo_Name,photo;
    ApiInterface apiInterface;
    DrawerLayout drawerLayout;
    ImageView toggledigital;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_volunteerlist, container, false);


        drawerLayout=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);
        toggledigital=(ImageView)v.findViewById(R.id.toggledigital);
        toggledigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });



        final RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView1);


        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        mobile = profile.get("Mobile");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");
        photo = profile.get("Photo");


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletRespons0> call = apiInterface.Statuss0(Id);
        call.enqueue(new Callback<WalletRespons0>() {
            @Override
            public void onResponse(Call<WalletRespons0> call, Response<WalletRespons0> response) {
                if (response.isSuccessful()) ;
                WalletRespons0 walletResponse1 = response.body();

                if (walletResponse1 != null) {
                    StatusDataResponse0 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();

                        List<Data0> data7 = walletResponse1.data;
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        RecyclerAdapter1 adapter = new RecyclerAdapter1(getActivity(), data7);
                        recyclerView.setAdapter(adapter);



                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public void onFailure(Call<WalletRespons0> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(getActivity(),
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });




        LinearLayout newVolunteer = (LinearLayout)v.findViewById(R.id.newVolunteer);
        newVolunteer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddVolunteer.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });


        return v;
    }
}

