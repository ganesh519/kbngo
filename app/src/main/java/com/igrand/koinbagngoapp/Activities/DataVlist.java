package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataVlist {


    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("middle_name")
    @Expose
    public Object middleName;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("mobile_number")
    @Expose
    public String mobileNumber;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("middleName", middleName).append("surname", surname).append("mobileNumber", mobileNumber).toString();
    }
}
