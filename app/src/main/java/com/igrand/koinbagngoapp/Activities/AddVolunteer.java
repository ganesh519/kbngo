package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVolunteer extends BaseActivity {

    Button next0;
    ImageView back15;
    PrefManager prefManager;
    String created_id;
    EditText titlev,firstnamev,surnamev,genderv,mobilenumberv,dobv,emailv;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_volunteer);

        next0=findViewById(R.id.next0);
        back15=findViewById(R.id.back15);

        titlev=findViewById(R.id.titlev);
        firstnamev=findViewById(R.id.firstnamev);
        surnamev=findViewById(R.id.surnamev);
        genderv=findViewById(R.id.genderv);
        mobilenumberv=findViewById(R.id.mobilenumberv);
        dobv=findViewById(R.id.dobv);
        emailv=findViewById(R.id.emailv);


        prefManager = new PrefManager(getApplicationContext());

       HashMap<String, String> profile = prefManager.getUserDetails();
        created_id = profile.get("id");






        back15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent=new Intent(AddVolunteer.this,Volunteers.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/

                finish();
            }
        });
        next0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = titlev.getText().toString();
                final String first_name = firstnamev.getText().toString();
                String surname = surnamev.getText().toString();
                final String mobile_number = mobilenumberv.getText().toString();
                String date_of_birth = dobv.getText().toString();
                String email = emailv.getText().toString();
                String gender = genderv.getText().toString();



                final ProgressDialog progressDialog = new ProgressDialog(AddVolunteer.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<AddVlist> call = apiInterface.AddVList1(created_id,title,first_name,surname,mobile_number,date_of_birth,email,gender);
                call.enqueue(new Callback<AddVlist>() {
                    @Override
                    public void onResponse(Call<AddVlist> call, Response<AddVlist> response) {
                        if (response.isSuccessful()) ;
                        AddVlist walletResponse1 = response.body();

                        if (walletResponse1 != null) {
                            StatusVlist statusDataResponse7 = walletResponse1.status;
                            if (statusDataResponse7.code == 200) {
                                progressDialog.dismiss();

                                DataVlist data7 = walletResponse1.data;
                                String mobile=data7.mobileNumber;
                                Intent intent=new Intent(AddVolunteer.this,OTP2.class);
                                intent.putExtra("MobileNumber",mobile);
                                intent.putExtra("FirstName",first_name);
                                startActivity(intent);


                            } else if (statusDataResponse7.code != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(AddVolunteer.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(AddVolunteer.this, "No Data Found...", Toast.LENGTH_SHORT).show();
                        }

                    }


                    @Override
                    public void onFailure(Call<AddVlist> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast = Toast.makeText(AddVolunteer.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });

            }
        });
    }
}
