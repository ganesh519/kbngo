package com.igrand.koinbagngoapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.LoginResponse1;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogActivity1 extends BaseActivity {
    ApiInterface apiInterface;
    LoginResponse1.StatusBean1 statusBean;
    PrefManager prefManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        prefManager=new PrefManager(CustomDialogActivity1.this);

        String mobile_number = getIntent().getStringExtra("Mobile");
        String password = getIntent().getStringExtra("Pass");

        if (!password.isEmpty()) {

            final Dialog progressDialog = new Dialog(CustomDialogActivity1.this, R.style.Theme_AppCompat_Light_NoActionBar);
            progressDialog.setContentView(R.layout.custom_dialog);
            Window window = progressDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
            animationView.setAnimation("refresh.json");
            animationView.playAnimation();
            animationView.loop(true);
            progressDialog.show();
            TextView text = (TextView) progressDialog.findViewById(R.id.text);
            ImageView image = (ImageView) progressDialog.findViewById(R.id.image);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoginResponse1> call = apiInterface.Statuss1(mobile_number, password);
            call.enqueue(new Callback<LoginResponse1>() {
                @Override
                public void onResponse(Call<LoginResponse1> call, Response<LoginResponse1> response) {


                    if (response.code() == 200) {

                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;

                        //Toast.makeText(WelcomeBack.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                        LoginResponse1.DataBean1 dataBean = response.body().getData();


                        String id = String.valueOf(dataBean.id);
                        String firstName = dataBean.firstName;
                        String surname = dataBean.surname;
                        String mobile_number = dataBean.mobileNumber;
                        String user_id = dataBean.userId;
                        String wallet_bal = dataBean.walletBal;
                        String ngo_name=dataBean.ngo_name;
                        String photo=dataBean.photo;

                        Intent intent = new Intent(CustomDialogActivity1.this, NavigationDrawerDashboard.class);
                        startActivity(intent);

                        prefManager.createLogin(id, firstName, surname, mobile_number, wallet_bal, user_id,ngo_name,photo);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(CustomDialogActivity1.this, "Please Enter Valid Password", Toast.LENGTH_SHORT).show();
                        finish();

                    }

                }

                @Override
                public void onFailure(Call<LoginResponse1> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(CustomDialogActivity1.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }

        else
        {
            Toast.makeText(getApplicationContext(), "Please enter Password", Toast.LENGTH_LONG).show();
        }
    }
}
