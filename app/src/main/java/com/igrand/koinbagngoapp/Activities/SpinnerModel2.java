package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class SpinnerModel2 {

    @SerializedName("status")
    @Expose
    public StatusSpinner2 status;
    @SerializedName("data")
    @Expose
    public List<DataSpinner2> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
