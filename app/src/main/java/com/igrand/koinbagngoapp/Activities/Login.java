package com.igrand.koinbagngoapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.LoginResponse;
import com.igrand.koinbagngoapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends BaseActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView login, koinbag, edit, txt1, txt2;
    EditText number;
    Button proceed;
    ApiInterface apiInterface;
    LoginResponse.StatusBean statusBean;
    String checking;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.loginto);
        koinbag = findViewById(R.id.koinbag);
        proceed = findViewById(R.id.proceed);
        edit = findViewById(R.id.edit);
        number = findViewById(R.id.number00);

        checking = getIntent().getStringExtra("Home");


        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkConnection();


            }

        });


    }

    private void checkConnection() {

        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {

        if (isConnected) {


            String mobile_number = number.getText().toString();

            Integer length = number.getText().length();


            if (mobile_number.equals("") || length != 10) {

                Toast toast = Toast.makeText(Login.this,
                        "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            } else {

                Intent intent = new Intent(Login.this, CustomDialogActivity.class);
                intent.putExtra("Mobile", mobile_number);
                startActivity(intent);
            }

        } else {

            // setContentView(R.layout.internet);

            Toast.makeText(Login.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

}


