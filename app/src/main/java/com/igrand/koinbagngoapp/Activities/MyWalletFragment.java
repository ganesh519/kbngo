package com.igrand.koinbagngoapp.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.WalletRequest;

import java.util.ArrayList;
import java.util.List;

public class MyWalletFragment extends Fragment {

    TabLayout tabs_wallet;
    ViewPager viewPager;
    ImageView back,walletRequest;
    ImageView toggledigital;
    DrawerLayout drawerLayout;
    TextView textview,textview1;
    Typeface typeface;
    ViewPagerAdapterNotifications pagerAdapterNotifications;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_ngowallet, container, false);



        drawerLayout=(DrawerLayout)getActivity().findViewById(R.id.drawer_layout);
        toggledigital=(ImageView)v.findViewById(R.id.toggle11);
        toggledigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        tabs_wallet = (TabLayout)v.findViewById(R.id.tabs_wallet);
        viewPager = (ViewPager)v.findViewById(R.id.viewPager);

        tabs_wallet.setSelectedTabIndicatorColor(Color.parseColor("#FFD71B"));
        tabs_wallet.setTabTextColors(Color.parseColor("#80F8F8F8"), Color.parseColor("#FFD71B"));


        pagerAdapterNotifications = new ViewPagerAdapterNotifications(getChildFragmentManager());
        pagerAdapterNotifications.addFragment(new AllFragment3(), "All");
        pagerAdapterNotifications.addFragment(new RequestsFragment2(), "Requests");
        pagerAdapterNotifications.addFragment(new ApprovedFragment2(), "Approved");
        pagerAdapterNotifications.addFragment(new RejectedFragment2(), "Rejected");

        viewPager.setAdapter(pagerAdapterNotifications);
        tabs_wallet.setupWithViewPager(viewPager);

        walletRequest=(ImageView)v.findViewById(R.id.walletRequest1);
        walletRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), WalletRequest.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        return v;

    }

    public class ViewPagerAdapterNotifications extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapterNotifications(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mFragmentTitleList.get(position);
            }

            public void addFragment(Fragment fragment, String title) {
                mFragmentList.add(fragment);
                mFragmentTitleList.add(title);
            }

        }

    }

