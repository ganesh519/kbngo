package com.igrand.koinbagngoapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.igrand.koinbagngoapp.R;

public class AddVolunteerDetails1 extends BaseActivity {

    Button submit0;
ImageView back19;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_volunteer_details1);

        submit0=findViewById(R.id.submit0);
        back19=findViewById(R.id.back19);
        back19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(AddVolunteerDetails1.this,AddVolunteerDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);*/
               finish();
            }
        });
        submit0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AddVolunteerDetails1.this,Volunteers.class);
                startActivity(intent);
            }
        });
    }
}
