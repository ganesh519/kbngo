package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataSpinner4 {


    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("country_id")
    @Expose
    public String countryId;
    @SerializedName("state_id")
    @Expose
    public String stateId;
    @SerializedName("district_id")
    @Expose
    public String districtId;
    @SerializedName("revenue_division_id")
    @Expose
    public String revenueDivisionId;
    @SerializedName("mandal_id")
    @Expose
    public String mandalId;
    @SerializedName("village_name")
    @Expose
    public String villageName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_id")
    @Expose
    public String createdId;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("countryId", countryId).append("stateId", stateId).append("districtId", districtId).append("revenueDivisionId", revenueDivisionId).append("mandalId", mandalId).append("villageName", villageName).append("status", status).append("createdId", createdId).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

}

