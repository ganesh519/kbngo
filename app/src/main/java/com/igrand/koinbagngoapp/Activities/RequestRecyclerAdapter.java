package com.igrand.koinbagngoapp.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.RecentTransaction;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.Proceesing;
import com.igrand.koinbagngoapp.ProfileImage;
import com.igrand.koinbagngoapp.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestRecyclerAdapter extends RecyclerView.Adapter<RequestRecyclerAdapter.Holder> {
    List<RecentTransaction> proceesingList;
    Context context;
    PrefManager prefManager;
    String id;
    ApiInterface apiInterface;

    public RequestRecyclerAdapter(Context context, List<RecentTransaction> proceesingList) {
        this.context=context;
        this.proceesingList=proceesingList;

    }

    @NonNull
    @Override
    public RequestRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listviewall, viewGroup, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestRecyclerAdapter.Holder holder, final int i) {

       /* holder.amount.setText(proceesingList.get(i).amount);
        holder.requested_date.setText(proceesingList.get(i).createdAt);*/

        String status1=proceesingList.get(i).status;

        if(status1.equals("2")) {
            holder.reject1.setTextColor(Color.parseColor("#1380BE"));
            holder.reject_amount.setVisibility(View.GONE);
            holder.money1.setText(proceesingList.get(i).amount);
            holder.request_date.setText(proceesingList.get(i).createdAt);
            holder.update_date.setVisibility(View.GONE);
            holder.rejectimage1.setVisibility(View.GONE);
            holder.reject1.setText("Waiting for Approval");
           // holder.reqcard.setEnabled(false);

        }   else if (status1.equals("1")) {

            holder.reject_amount.setVisibility(View.VISIBLE);
            holder.rejectimage1.setVisibility(View.VISIBLE);
            holder.rejectimage1.setImageResource(R.drawable.approved);
            holder.reject1.setText("Approved Money");
            holder.reject1.setTextColor(Color.parseColor("#00A219"));
            holder.reject_amount.setText(proceesingList.get(i).amount);
            holder.money1.setText(proceesingList.get(i).amount);
            holder.request_date.setText(proceesingList.get(i).createdAt);
            holder.update_date.setVisibility(View.VISIBLE);
            holder.reqcard.setEnabled(false);
           // holder.update_date.setText(proceesingList.get(i).updatedDate);

        }
        else if (status1.equals("0")) {
            holder.reject_amount.setVisibility(View.VISIBLE);
            holder.rejectimage1.setVisibility(View.VISIBLE);
            holder.reject1.setText("Rejected");
            holder.rejectimage1.setImageResource(R.drawable.rejected);
            holder.reject1.setTextColor(Color.parseColor("#FF0000"));
            holder.reject_amount.setText(proceesingList.get(i).amount);
            holder.money1.setText(proceesingList.get(i).amount);
            holder.request_date.setText(proceesingList.get(i).createdAt);
            holder.update_date.setVisibility(View.VISIBLE);
            holder.reqcard.setEnabled(false);
           // holder.update_date.setText(proceesingList.get(i).updatedDate);
        }




        holder.i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox2);
                dialog.show();
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                final TextView reqdate1, reqrs1, reqdes1, appdes, appdate, reqrej;
                final ImageView image2, rejectimage, dot1, dot3;
                final CardView linear6;
                final LinearLayout linear5;

                final View dot2;
                final RelativeLayout relativelayout;

                reqdate1 = dialog.findViewById(R.id.reqdate12);
                reqrs1 = dialog.findViewById(R.id.reqrs12);
                reqdes1 = dialog.findViewById(R.id.reqdes12);
                //image200=dialog.findViewById(R.id.image200);
                appdes = dialog.findViewById(R.id.reqrejdes12);
                appdate = dialog.findViewById(R.id.reqrejdate12);
                image2 = dialog.findViewById(R.id.image2);
                rejectimage = dialog.findViewById(R.id.rejectimage);
                reqrej = dialog.findViewById(R.id.reqrej);
                linear5 = dialog.findViewById(R.id.linear50);
                linear6 = dialog.findViewById(R.id.linear60);
                /*dot1=dialog.findViewById(R.id.dot1);
                dot3=dialog.findViewById(R.id.dot3);
                dot2=dialog.findViewById(R.id.dot2);*/
                relativelayout = dialog.findViewById(R.id.relativelayout);


                prefManager = new PrefManager(context);
                HashMap<String, String> profile = prefManager.getUserDetails();
                id = profile.get("id");
                String request_id = proceesingList.get(i).id;

                final ProgressDialog progressDialog1 = new ProgressDialog(context);
                progressDialog1.setMessage("Loading.....");
                progressDialog1.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StatusResponse10> call1 = apiInterface.Statuss10(id, request_id);
                call1.enqueue(new Callback<StatusResponse10>() {
                    @Override
                    public void onResponse(Call<StatusResponse10> call, Response<StatusResponse10> response) {
                        if (response.isSuccessful()) ;

                        StatusResponse10 statusResponse = response.body();
                        if (statusResponse != null) {

                            StatusDataResponse10 statusDataResponse = statusResponse.status;

                            if (statusDataResponse.code == 200) {
                                progressDialog1.dismiss();
                                relativelayout.setVisibility(View.VISIBLE);

                                Data10 data = statusResponse.data;
                                String amount = data.amount;
                                String created_at = data.createdAt;
                                String updated_at = data.updatedAt;
                                String description = data.description;
                                String attachments = data.attachments;
                                String updateDescription = data.updateDescription;


                                String status1 = data.status;

                                if (status1.equals("0")) {

                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });

                                } else if (status1.equals("1")) {

                                    reqrej.setText("Approved");
                                    reqrej.setTextColor(Color.parseColor("#00A219"));
                                    rejectimage.setImageResource(R.drawable.approved);
                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });

                                } else if (status1.equals("2")) {


                                    linear5.setVisibility(View.GONE);
                                    linear6.setVisibility(View.GONE);
                               /* dot1.setVisibility(View.GONE);
                                dot2.setVisibility(View.GONE);
                                dot3.setVisibility(View.GONE);*/
                                    reqdate1.setText(created_at);
                                    reqrs1.setText(amount);
                                    reqdes1.setText(description);
                                    appdes.setText(updateDescription);
                                    appdate.setText(updated_at);
                                    Picasso.get().load(attachments).into(image2);

                                    image2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {


                                            image2.buildDrawingCache();
                                            Bitmap bitmap = image2.getDrawingCache();
                                            Intent intent = new Intent(context, ProfileImage.class);

                                            ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                            bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                            intent.putExtra("byteArray", _bs.toByteArray());
                                            context.startActivity(intent);

                                        }
                                    });


                                }


                            } else if (statusDataResponse.code == 409) {
                                progressDialog1.dismiss();
                                Toast.makeText(context, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }




                    @Override
                    public void onFailure(Call<StatusResponse10> call, Throwable t) {
                        progressDialog1.dismiss();
                        //Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();

                        Toast toast = Toast.makeText(context,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }

                });






                ImageView close=(ImageView) dialog.findViewById(R.id.close1);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

            }

        });





        holder.reqcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox4);
                dialog.show();
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                TextView rejectbutton,approvebutton,volname,volname1,volid,volamount,rs1;
                final ImageView volpic;
                LinearLayout dialog1;

                rejectbutton=dialog.findViewById(R.id.rejectbutton);
                approvebutton=dialog.findViewById(R.id.approvebutton);
                dialog1=dialog.findViewById(R.id.dialog);
                volname=dialog.findViewById(R.id.volname);
                volid=dialog.findViewById(R.id.volid);
                volamount=dialog.findViewById(R.id.volamount);
                volpic=dialog.findViewById(R.id.volpic);
                rs1=dialog.findViewById(R.id.rs1);
                volname1=dialog.findViewById(R.id.volname1);

                    String pic=proceesingList.get(i).photo;


                volname.setText(proceesingList.get(i).firstName);
                volname1.setText(proceesingList.get(i).surname);
                volid.setText(proceesingList.get(i).resquestedBy);
                volamount.setText(proceesingList.get(i).amount);
                rs1.setText(proceesingList.get(i).amount);

                Picasso.get().load(pic).into(volpic);


                volpic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        volpic.buildDrawingCache();
                        Bitmap bitmap = volpic.getDrawingCache();
                        Intent intent = new Intent(context, ProfileImage.class);

                        ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                        intent.putExtra("byteArray", _bs.toByteArray());
                        context.startActivity(intent);
                    }
                });





                rejectbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int status=0;
                        String id=proceesingList.get(i).id;


                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<Approve> call = apiInterface.approve(id,status);
                        call.enqueue(new Callback<Approve>() {
                            @Override
                            public void onResponse(Call<Approve> call, Response<Approve> response) {
                                if (response.isSuccessful()) ;

                                Approve walletResponse1 = response.body();
                                if (walletResponse1!=null) {
                                    StatusDataResponseapprove statusDataResponse7 = walletResponse1.status;
                                    if (statusDataResponse7.code == 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();


                                    }else if (statusDataResponse7.code != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                                    }

                                }

                                else {

                                    progressDialog.dismiss();
                                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<Approve> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                                Toast toast= Toast.makeText(context,
                                        t.getMessage() , Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();



                            }
                        });




                    }
                });

                approvebutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        int status=1;
                        String id=proceesingList.get(i).id;


                        final ProgressDialog progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();

                        apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<Approve> call = apiInterface.approve(id,status);
                        call.enqueue(new Callback<Approve>() {
                            @Override
                            public void onResponse(Call<Approve> call, Response<Approve> response) {
                                 if (response.isSuccessful()) ;

                                Approve walletResponse1 = response.body();

                                if (walletResponse1!=null) {
                                    StatusDataResponseapprove statusDataResponse7 = walletResponse1.status;
                                    if (statusDataResponse7.code == 200) {
                                        progressDialog.dismiss();
                                        dialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();


                                    }
                                    else if (statusDataResponse7.code != 200) {
                                        progressDialog.dismiss();
                                        Toast.makeText(context, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                                    }

                                }

                                else {

                                    progressDialog.dismiss();
                                    Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();

                                }

                            }

                            @Override
                            public void onFailure(Call<Approve> call, Throwable t) {
                                progressDialog.dismiss();
                                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                                Toast toast= Toast.makeText(context,
                                        t.getMessage() , Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                                toast.show();



                            }
                        });


                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return proceesingList.size();
    }

   public class Holder extends RecyclerView.ViewHolder{
        TextView request_date,amount,reject_amount,update_date,money1,reject1;
        ImageView i1,rejectimage1;
        CardView card0,reqcard;
        public Holder(@NonNull View itemView) {
            super(itemView);

            reject_amount=itemView.findViewById(R.id.reject_amount110);
            request_date=itemView.findViewById(R.id.request_date1);
            update_date=itemView.findViewById(R.id.update_date1);
            money1=itemView.findViewById(R.id.money110);
            i1=itemView.findViewById(R.id.i1110);
            reject1=itemView.findViewById(R.id.reject110);
            rejectimage1=itemView.findViewById(R.id.rejectimage110);
            card0=itemView.findViewById(R.id.card0);
            reqcard=itemView.findViewById(R.id.reqcard);


        }
    }
}
