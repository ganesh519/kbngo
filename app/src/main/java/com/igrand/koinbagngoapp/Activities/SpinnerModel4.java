package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class SpinnerModel4 {


    @SerializedName("status")
    @Expose
    public StatusSpinner4 status;
    @SerializedName("data")
    @Expose
    public List<DataSpinner4> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
