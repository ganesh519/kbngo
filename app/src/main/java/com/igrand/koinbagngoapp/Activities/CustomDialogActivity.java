package com.igrand.koinbagngoapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.LoginResponse;
import com.igrand.koinbagngoapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogActivity extends BaseActivity {
    ApiInterface apiInterface;
    LoginResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        String mobile_number = getIntent().getStringExtra("Mobile");

        if (!mobile_number.isEmpty()) {

            final Dialog progressDialog = new Dialog(CustomDialogActivity.this, R.style.Theme_AppCompat_Light_NoActionBar);
            progressDialog.setContentView(R.layout.custom_dialog);
            Window window = progressDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            LottieAnimationView animationView = progressDialog.findViewById(R.id.lottie_view);
            animationView.setAnimation("refresh.json");
            animationView.playAnimation();
            animationView.loop(true);
            progressDialog.show();
            TextView text = (TextView) progressDialog.findViewById(R.id.text);
            ImageView image = (ImageView) progressDialog.findViewById(R.id.image);


            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoginResponse> call = apiInterface.Login(mobile_number);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                    if (response.code() == 200) {


                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                        LoginResponse.DataBean dataBean = response.body().getData();
                        Intent intent = new Intent(CustomDialogActivity.this, WelcomeBack.class);
                        intent.putExtra("Name1", dataBean.getFirstName());
                        intent.putExtra("SurName1", dataBean.getSurname());
                        intent.putExtra("Mobile_number", dataBean.getMobileNumber());
                        progressDialog.dismiss();
                        startActivity(intent);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(CustomDialogActivity.this, "Volunteer Not Exist ,Please Contact Koinbag Ngo..", Toast.LENGTH_SHORT).show();
                        finish();

                    }


                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getApplicationContext(), "Please enter MobileNumber", Toast.LENGTH_LONG).show();
        }


    }

}

