package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.Data001;
import com.igrand.koinbagngoapp.Fragments.StatusDataResponse001;
import com.igrand.koinbagngoapp.Fragments.WalletResponse001;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllFragment3 extends Fragment {
    ApiInterface apiInterface;
    RecyclerView recycler_allview;
    PrefManager prefManager;
    String Id;
    AllRecyclerAdapter2 allRecyclerAdapter2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_all, container, false);

        recycler_allview=v.findViewById(R.id.recycler_allview);
        /*recycler_reject=v.findViewById(R.id.recycler_reject);
        recycler_approved=v.findViewById(R.id.recycler_approved);
*/
        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");


        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletResponse001> call = apiInterface.Statuss001(Id);
        call.enqueue(new Callback<WalletResponse001>() {
            @Override
            public void onResponse(Call<WalletResponse001> call, Response<WalletResponse001> response) {
                // if (response.isSuccessful()) ;
                WalletResponse001 walletResponse1 = response.body();
                if (walletResponse1 != null) {


                    StatusDataResponse001 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();


                        List<Data001> data7 = walletResponse1.data;


                        recycler_allview.setLayoutManager(new LinearLayoutManager(getActivity()));
                        allRecyclerAdapter2 = new AllRecyclerAdapter2(getContext(), data7);
                        recycler_allview.setAdapter(allRecyclerAdapter2);

                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No Data", Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<WalletResponse001> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });



        return v;
    }
}

