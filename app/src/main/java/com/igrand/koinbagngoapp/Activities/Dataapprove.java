package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Dataapprove {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("amount")
    @Expose
    public String amount;
    @SerializedName("attachments")
    @Expose
    public String attachments;
    @SerializedName("resquested_to")
    @Expose
    public String resquestedTo;
    @SerializedName("resquested_by")
    @Expose
    public String resquestedBy;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("payee_remarks")
    @Expose
    public String payeeRemarks;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;
    @SerializedName("paymentrequest_id")
    @Expose
    public Object paymentrequestId;
    @SerializedName("bank_details")
    @Expose
    public Object bankDetails;
    @SerializedName("ref_number")
    @Expose
    public Object refNumber;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("amount", amount).append("attachments", attachments).append("resquestedTo", resquestedTo).append("resquestedBy", resquestedBy).append("description", description).append("payeeRemarks", payeeRemarks).append("status", status).append("createdAt", createdAt).append("updatedAt", updatedAt).append("paymentrequestId", paymentrequestId).append("bankDetails", bankDetails).append("refNumber", refNumber).toString();
    }
}
