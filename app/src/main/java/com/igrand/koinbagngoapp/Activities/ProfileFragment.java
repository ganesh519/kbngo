package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.Data5;
import com.igrand.koinbagngoapp.Models.StatusDataResponse5;
import com.igrand.koinbagngoapp.Models.StatusResponse5;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.ProfileImage;
import com.igrand.koinbagngoapp.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    DrawerLayout drawerLayout;
    ApiInterface apiInterface;
    ImageView toggledigital, back77, photo1, aadharphoto1, panphoto1, tenth1, inter1,inter001;
    TextView titleprofile1, lastname1, surname1, middlename1, dob1, mobilenumber1, email1, state1, revenuedivision1, district1, mandal1,
            village1, addressline11, addressline21, addressline31, pancard1, aadharcard1, name1, userid1, emailid1,mobilenumber2,
            tenthmarks1,intermarks1,degreemarks1,contactname1,contacttype1,contactnumber1,relation1,mobilenumber3;
    String Id,f_name,surname,email,mobile,walletbals,userid,Photo;
    PrefManager prefManager;
    LinearLayout linear0001,linear000;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);


        prefManager=new PrefManager(getActivity());
        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");
        mobile=profile.get("Mobile");
        userid=profile.get("Userid");
        walletbals=profile.get("Walletbals");
        Photo=profile.get("Photo");

        final ProgressDialog progressDialog1 = new ProgressDialog(getContext());
        progressDialog1.setMessage("Loading.....");
        progressDialog1.show();


        aadharphoto1=(ImageView)v.findViewById(R.id.aadharphoto);
        name1=(TextView) v.findViewById(R.id.name);
        userid1=(TextView) v.findViewById(R.id.userid);
        emailid1=(TextView)v.findViewById(R.id.email);
        panphoto1=(ImageView) v.findViewById(R.id.panphoto);
        photo1=(ImageView) v.findViewById(R.id.photo);
        titleprofile1=(TextView)v.findViewById(R.id.titleprofile);
        lastname1=(TextView) v.findViewById(R.id.lastname);
        surname1=(TextView) v.findViewById(R.id.surname);
        middlename1=(TextView) v.findViewById(R.id.middlename);
        dob1=(TextView) v.findViewById(R.id.dob);
        mobilenumber1=(TextView) v.findViewById(R.id.mobilenumber);
        email1=(TextView) v.findViewById(R.id.emailid);
        state1=(TextView) v.findViewById(R.id.state);
        revenuedivision1=(TextView) v.findViewById(R.id.revenuedivision);
        district1=(TextView) v.findViewById(R.id.district);
        mandal1=(TextView) v.findViewById(R.id.mandal);
        village1=(TextView) v.findViewById(R.id.village);
        addressline11=(TextView) v.findViewById(R.id.addressline1);
        addressline21=(TextView) v.findViewById(R.id.addressline2);
        addressline31=(TextView) v.findViewById(R.id.addressline3);
        pancard1=(TextView) v.findViewById(R.id.pancard);
        aadharcard1=(TextView) v.findViewById(R.id.aadharcard);
        tenth1=(ImageView) v.findViewById(R.id.tenth);
        inter1=(ImageView) v.findViewById(R.id.inter);
        mobilenumber3=(TextView) v.findViewById(R.id.mobilenumber3);
        linear0001=(LinearLayout) v.findViewById(R.id.linear0001);
        linear000=(LinearLayout) v.findViewById(R.id.linear000);
        tenthmarks1=(TextView) v.findViewById(R.id.tenthmarks);
        intermarks1=(TextView) v.findViewById(R.id.intermarks);
        degreemarks1=(TextView) v.findViewById(R.id.degreemarks);
        contactname1=(TextView) v.findViewById(R.id.contactname);
        contactnumber1=(TextView) v.findViewById(R.id.contactnumber);
        contacttype1=(TextView) v.findViewById(R.id.contacttype);
        relation1=(TextView) v.findViewById(R.id.relation);
        mobilenumber2=(TextView) v.findViewById(R.id.mobilenumber);
        mobilenumber3=(TextView) v.findViewById(R.id.mobilenumber3);




        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse5> call1 = apiInterface.Statuss5(Id);
        call1.enqueue(new Callback<StatusResponse5>() {
            @Override
            public void onResponse(Call<StatusResponse5> call, Response<StatusResponse5> response) {
                if (response.isSuccessful()) ;

                StatusResponse5 statusResponse = response.body();
                StatusDataResponse5 statusDataResponse = statusResponse.status;

                if (statusDataResponse.code == 200) {
                    progressDialog1.dismiss();
                    linear000.setVisibility(View.VISIBLE);

                    Data5 data = statusResponse.data;
                    String title = data.title;
                    String surname = data.surname;
                    String last_name = data.lastName;
                    String middle_name = data.middleName;
                    String email = data.email;
                    String photo = data.photo;
                    String mobile_number = data.mobileNumber;
                    String date_of_birth = data.dateOfBirth;
                    String state = data.state;
                    String district = data.district;
                    String revenue_devision = data.revenueDevision;
                    String mandal = data.mandal;
                    String village = data.village;
                    String addressline1 = data.addressline1;
                    String addressline2 = data.addressline2;
                    String addressline3 = data.addressline3;
                    String pancard = data.pancard;
                    String panphoto = data.panphoto;
                    String aadharcard = data.aadharcard;
                    String aadharphoto = data.aadharphoto;
                    String _10th = data.tenthMarks;
                    String inter = data.interMarks;
                    String firstname=data.firstName;
                    String tenthmarks=data.tenthMarks;
                    String intermarks=data.interMarks;
                    String degreemarks=data.degreeMarks;
                    String contactname=data.contactName;
                    String contacttype=data.contactType;
                    String contactnumber=data.contactNumber;
                    String relation=data.relation;
                    String tenthphoto=data.tenthPhoto;
                    String interphoto=data.interPhoto;

                    userid1.setText(userid);
                    name1.setText(f_name);
                    email1.setText(email);
                    titleprofile1.setText(title);
                    surname1.setText(surname);
                    lastname1.setText(firstname);
                    middlename1.setText(middle_name);
                    emailid1.setText(email);
                    mobilenumber1.setText(mobile_number);
                    dob1.setText(date_of_birth);
                    state1.setText(state);
                    district1.setText(district);
                    revenuedivision1.setText(revenue_devision);
                    mandal1.setText(mandal);
                    village1.setText(village);
                    addressline11.setText(addressline1);
                    addressline21.setText(addressline2);
                    addressline31.setText(addressline3);
                    pancard1.setText(pancard);
                    aadharcard1.setText(aadharcard);
                    mobilenumber2.setText(mobile_number);
                    mobilenumber3.setText(mobile_number);


                    tenthmarks1.setText(tenthmarks);
                    intermarks1.setText(intermarks);
                    degreemarks1.setText(degreemarks);
                    contactname1.setText(contactname);
                    contactnumber1.setText(contactnumber);
                    // photo1.conta(Integer.parseInt(photo));
                    contacttype1.setText(contacttype);
                    relation1.setText(relation);



                    // Picasso.get().load("http://igranddeveloper.live/kbig/" + photo).error(R.drawable.image).into(photo1);
                    Picasso.get().load(panphoto).error(R.drawable.kb).into(panphoto1);
                    Picasso.get().load(aadharphoto).error(R.drawable.kb).into(aadharphoto1);
                    Picasso.get().load(tenthphoto).error(R.drawable.kb).into(tenth1);
                    Picasso.get().load(interphoto).error(R.drawable.kb).into(inter1);

                    if(photo!=null){
                        Picasso.get().load(photo).error(R.drawable.kb).into(photo1);
                    } else {
                        photo1.setImageResource(R.drawable.kb);
                    }

                    photo1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            photo1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    photo1.buildDrawingCache();
                                    Bitmap bitmap = photo1.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);

                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });


                    panphoto1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            panphoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    panphoto1.buildDrawingCache();
                                    Bitmap bitmap = panphoto1.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);

                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });


                    aadharphoto1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            aadharphoto1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    aadharphoto1.buildDrawingCache();
                                    Bitmap bitmap = aadharphoto1.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);

                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });


                  /*  inter001.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            inter001.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    inter001.buildDrawingCache();
                                    Bitmap bitmap = inter001.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);
                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });*/

                    tenth1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            tenth1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    tenth1.buildDrawingCache();
                                    Bitmap bitmap = tenth1.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);

                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });

                    inter1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            inter1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    inter1.buildDrawingCache();
                                    Bitmap bitmap = inter1.getDrawingCache();
                                    Intent intent=new Intent(getActivity(), ProfileImage.class);

                                    ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                                    intent.putExtra("byteArray", _bs.toByteArray());
                                    startActivity(intent);

                                }
                            });

                        }
                    });
                    //Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    //    Intent intent = new Intent(NavigationDrawerDashboard.this, PersonalDetails.class);

                } else if (statusDataResponse.code == 409) {
                    progressDialog1.dismiss();
                    Toast.makeText(getContext(), statusDataResponse.message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse5> call, Throwable t) {
                progressDialog1.dismiss();
                //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast= Toast.makeText(getActivity(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });

        drawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        toggledigital = (ImageView) v.findViewById(R.id.toggleprofile);
        toggledigital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        return v;
    }


}
