package com.igrand.koinbagngoapp.Activities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataSpinner {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("country_id")
    @Expose
    public String countryId;
    @SerializedName("state_name")
    @Expose
    public String stateName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_id")
    @Expose
    public String createdId;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("countryId", countryId).append("stateName", stateName).append("status", status).append("createdId", createdId).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }
}
