package com.igrand.koinbagngoapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Adapters.RecyclerAdapter1;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Fragments.Data0;
import com.igrand.koinbagngoapp.Fragments.StatusDataResponse0;
import com.igrand.koinbagngoapp.Fragments.WalletRespons0;
import com.igrand.koinbagngoapp.PrefManager;
import com.igrand.koinbagngoapp.R;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Volunteers extends BaseActivity {

    RecyclerView recyclerView;
    LinearLayout newVolunteer;
    ImageView back12;
    PrefManager prefManager;
    String Id, f_name, mobile, userid, walletbals, photo;
    ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteers);
        back12 = findViewById(R.id.back12);
        recyclerView = findViewById(R.id.recyclerView1);
        back12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Volunteers.this, NavigationDrawerDashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                //finish();
            }
        });


        prefManager = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = prefManager.getUserDetails();
        Id = profile.get("id");
        f_name = profile.get("f_name");
        mobile = profile.get("Mobile");
        userid = profile.get("Userid");
        walletbals = profile.get("Walletbals");
        photo = profile.get("Photo");


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletRespons0> call = apiInterface.Statuss0(Id);
        call.enqueue(new Callback<WalletRespons0>() {
            @Override
            public void onResponse(Call<WalletRespons0> call, Response<WalletRespons0> response) {
                if (response.isSuccessful()) ;
                WalletRespons0 walletResponse1 = response.body();

                if (walletResponse1 != null) {
                    StatusDataResponse0 statusDataResponse7 = walletResponse1.status;
                    if (statusDataResponse7.code == 200) {
                        progressDialog.dismiss();

                        List<Data0> data7 = walletResponse1.data;
                        recyclerView.setLayoutManager(new LinearLayoutManager(Volunteers.this));
                        RecyclerAdapter1 adapter = new RecyclerAdapter1(Volunteers.this, data7);
                        recyclerView.setAdapter(adapter);


                    } else if (statusDataResponse7.code != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(Volunteers.this, statusDataResponse7.message, Toast.LENGTH_SHORT).show();

                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(Volunteers.this, "No Data Found...", Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public void onFailure(Call<WalletRespons0> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

                Toast toast = Toast.makeText(Volunteers.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });




        newVolunteer = findViewById(R.id.newVolunteer);
        newVolunteer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Volunteers.this, AddVolunteer.class);
                startActivity(intent);
            }
        });

    }
}
