package com.igrand.koinbagngoapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.igrand.koinbagngoapp.Activities.Data00;
import com.igrand.koinbagngoapp.Activities.Data14;
import com.igrand.koinbagngoapp.Models.StatusDataResponse7;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class WalletResponse0 {

    @SerializedName("status")
    @Expose
    public StatusDataResponse000 status;
    @SerializedName("data")
    @Expose
    public List<Data000> data = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("data", data).toString();
    }
}
