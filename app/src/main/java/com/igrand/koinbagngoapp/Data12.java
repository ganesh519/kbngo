package com.igrand.koinbagngoapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Data12 {

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).toString();
    }
}
