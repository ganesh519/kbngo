package com.igrand.koinbagngoapp.Adapters;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.koinbagngoapp.Activities.NavigationDrawerDashboard;
import com.igrand.koinbagngoapp.Calling;
import com.igrand.koinbagngoapp.Fragments.Data0;
import com.igrand.koinbagngoapp.ProfileImage;
import com.igrand.koinbagngoapp.R;
import com.igrand.koinbagngoapp.Activities.VolunteersDetails;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.ViewHolderClass> {

    private static final int REQUEST_PHONE_CALL = 1;
    Context context;
    List<Data0> data0;



    public RecyclerAdapter1(Context context, List<Data0> data0) {

        this.context = context;
        this.data0 = data0;
    }


    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view = LayoutInflater.from(context).inflate(R.layout.recycler_list1, viewGroup, false);
        ViewHolderClass viewHolderClass = new ViewHolderClass(view);
        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderClass viewHolderClass, final int i) {

        final String id=data0.get(i).id;
        final String firstname = data0.get(i).firstName;
        String lastname = data0.get(i).surname;
        final String walletbal = data0.get(i).walletBalance;
        final String trans = data0.get(i).totalTransaction;
        final String comm = data0.get(i).commission;
        final String pic = data0.get(i).photo;
        final String number = data0.get(i).mobileNumber;
        //Picasso.get().load(pic).into(volunteerpic);


        viewHolderClass.volunteerfirstname.setText(firstname);
        viewHolderClass.volunteername.setText(lastname);
        viewHolderClass.volunteerwalletbalance.setText(walletbal);
        viewHolderClass.volunteertransactions.setText(trans);
        viewHolderClass.volunteercommission.setText(comm);

        viewHolderClass.callimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialogbox7);
                dialog.show();
                //dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                              TextView callname,callnumber;
                ImageView callpic;
                Button callbutton;

                callname = dialog.findViewById(R.id.callname);
                callnumber = dialog.findViewById(R.id.callnumber);
                callpic = dialog.findViewById(R.id.callpic);
                callbutton = dialog.findViewById(R.id.callbutton);


                callname.setText(firstname);
                callnumber.setText(number);
                Picasso.get().load(pic).into(callpic);


callbutton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" +number));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            // ActivityCompat.requestPermissions(Context, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {

            context.startActivity(intent);
        }


    }
});


            }
        });


        Picasso.get().load(pic).into(viewHolderClass.volunteerpic);

        viewHolderClass.volunteerpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolderClass.volunteerpic.buildDrawingCache();
                Bitmap bitmap = viewHolderClass.volunteerpic.getDrawingCache();
                Intent intent = new Intent(context, ProfileImage.class);

                ByteArrayOutputStream _bs = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, _bs);
                intent.putExtra("byteArray", _bs.toByteArray());
                context.startActivity(intent);
            }
        });

        viewHolderClass.ngocard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,VolunteersDetails.class);
                intent.putExtra("Id",id);
                intent.putExtra("Wallet",walletbal);
                intent.putExtra("Trans",trans);
                intent.putExtra("Comm",comm);
                intent.putExtra("Pic",pic);
                intent.putExtra("Num",number);
                intent.putExtra("Name",firstname);

                context.startActivity(intent);
            }
        });




    }

    @Override
    public int getItemCount() {
        return data0.size();
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        ImageView  volunteerpic,callimage;
        TextView volunteerfirstname,volunteername,volunteerwalletbalance,volunteertransactions,volunteercommission;
        CardView ngocard;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();


            volunteerfirstname=itemView.findViewById(R.id.volunteerfirstname);
            volunteername=itemView.findViewById(R.id.volunteername);
            volunteerwalletbalance=itemView.findViewById(R.id.volunteerwallet);
            volunteertransactions=itemView.findViewById(R.id.volunteertransactions);
            volunteercommission=itemView.findViewById(R.id.volunteercommissions);
            volunteerpic=itemView.findViewById(R.id.volunteerpic);
            callimage=itemView.findViewById(R.id.callimage);
            ngocard=itemView.findViewById(R.id.ngocard);




        }
    }
}
