package com.igrand.koinbagngoapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagngoapp.Fragments.AllFragment;
import com.igrand.koinbagngoapp.Fragments.ApprovedFragment;
import com.igrand.koinbagngoapp.Fragments.RejectFragment;
import com.igrand.koinbagngoapp.Fragments.RequestFragment;

public class MyAdapter1 extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;


    public MyAdapter1(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }



    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AllFragment allFragment = new AllFragment();
                return allFragment;
            case 1:
                RequestFragment requestFragment = new RequestFragment();
                return requestFragment;
            case 2:
                ApprovedFragment approvedFragment = new ApprovedFragment();
                return approvedFragment;

            case 3:
                RejectFragment rejectFragment = new RejectFragment();
                return rejectFragment;

            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}

