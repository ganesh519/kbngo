package com.igrand.koinbagngoapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.koinbagngoapp.Models.Data7;
import com.igrand.koinbagngoapp.R;

import java.util.List;

public class RecyclerAdapterWallet extends RecyclerView.Adapter<RecyclerAdapterWallet.ViewHolderClass> {

    List<Data7> data7;



    Context context;
    // String amount,requesteddate;

    public RecyclerAdapterWallet(Context context, List<Data7> walletdata) {

        this.context=context;
        this.data7=walletdata;
    }

    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.listview1,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolderClass viewHolderClass, int i) {

        /* Data7 data=data7.get(i);*/

       /* viewHolderClass.requesteddate.setText(data7.get(i).createdAt);
        viewHolderClass.amount.setText(data7.get(i).amount);*/

    }


    @Override
    public int getItemCount() {
        return data7.size();
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        TextView requesteddate,amount;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();

            requesteddate=itemView.findViewById(R.id.requested_date);
            amount=itemView.findViewById(R.id.amount);

        }
    }
}



