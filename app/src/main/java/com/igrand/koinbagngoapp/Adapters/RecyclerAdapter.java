package com.igrand.koinbagngoapp.Adapters;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.igrand.koinbagngoapp.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolderClass> {

    Context context;

    public RecyclerAdapter(FragmentActivity activity) {
        this.context=activity;
    }


    @NonNull
    @Override
    public RecyclerAdapter.ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view= LayoutInflater.from(context).inflate(R.layout.recycler_list,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapter.ViewHolderClass viewHolderClass, int i) {

            viewHolderClass.cardView5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialogbox4);
                    dialog.show();
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                }
            });

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;
        CardView cardView5;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            cardView5=itemView.findViewById(R.id.cardView5);


        }
    }
}
