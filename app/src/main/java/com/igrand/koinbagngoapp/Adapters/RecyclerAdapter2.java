package com.igrand.koinbagngoapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igrand.koinbagngoapp.R;

public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.ViewHolderClass> {

    Context context;

    public RecyclerAdapter2(FragmentActivity activity) {
        this.context=activity;
    }


    @NonNull
    @Override
    public ViewHolderClass onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View view= LayoutInflater.from(context).inflate(R.layout.recycler_list2,viewGroup,false);
        ViewHolderClass viewHolderClass=new ViewHolderClass(view);
        return viewHolderClass;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderClass viewHolderClass, int i) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public static class ViewHolderClass extends RecyclerView.ViewHolder {

        private final Context context;

        public ViewHolderClass(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();



        }
    }
}
