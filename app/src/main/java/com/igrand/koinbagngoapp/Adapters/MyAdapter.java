package com.igrand.koinbagngoapp.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.igrand.koinbagngoapp.Fragments.DigitalFragment;
import com.igrand.koinbagngoapp.Fragments.WalletFragment;

public class MyAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    public MyAdapter(Context context, FragmentManager supportFragmentManager, int tabCount) {

        super(supportFragmentManager);
        myContext = context;
        this.totalTabs = tabCount;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                DigitalFragment digitalFragment = new DigitalFragment();
                return digitalFragment;
            case 1:
                WalletFragment walletFragment = new WalletFragment();
                return walletFragment;


            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;

    }
}
