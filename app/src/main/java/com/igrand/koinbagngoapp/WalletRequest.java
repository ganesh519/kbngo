package com.igrand.koinbagngoapp;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.koinbagngoapp.Activities.BaseActivity;
import com.igrand.koinbagngoapp.Activities.StatusResponse10;
import com.igrand.koinbagngoapp.Client.ApiClient;
import com.igrand.koinbagngoapp.Client.ApiInterface;
import com.igrand.koinbagngoapp.Models.StatusDataResponse8;
import com.igrand.koinbagngoapp.Models.StatusResponse8;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletRequest extends BaseActivity {

    private static final String TAG = "wallet";
    Button submit1,ok;
    LinearLayout upload1;
    TextView walletreq,ngoname,jaden;
    EditText amount1,description1;
    ApiInterface apiInterface;
    ImageView backpage2001,imagee1,imagee2,imagee3,imageupload1;
    PrefManager prefManager;
    String Id,f_name,surname,email,mobile,walletbals,userid,attachments,Ngo_Name;
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic,imagepath,picturePath;
    File file=null;
    MultipartBody.Part body;


    public static final int REQUEST_CODE = 1;


    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_request);
        submit1=findViewById(R.id.submit);






        walletreq=findViewById(R.id.walletreq);
        ngoname=findViewById(R.id.ngoname);
        jaden=findViewById(R.id.jaden);
        backpage2001=findViewById(R.id.backpage2001);


        imageupload1=findViewById(R.id.imageupload1);

        backpage2001.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prefManager=new PrefManager(getApplicationContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        Id=profile.get("id");
        f_name=profile.get("f_name");
        surname=profile.get("Surname");
        mobile=profile.get("Mobile");
        userid=profile.get("Userid");
        walletbals=profile.get("Walletbals");
        Ngo_Name = profile.get("NgoName");
        jaden.setText(Ngo_Name);

        amount1=findViewById(R.id.amount2);
        description1=findViewById(R.id.description2);




        submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (ContextCompat.checkSelfPermission(WalletRequest.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(WalletRequest.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(WalletRequest.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(WalletRequest.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(WalletRequest.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(WalletRequest.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(WalletRequest.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(WalletRequest.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }





                final String amount=amount1.getText().toString();
                String description=description1.getText().toString();

                if (amount.equals(""))
                {
                    Toast.makeText(WalletRequest.this, "Enter Amount..", Toast.LENGTH_SHORT).show();
                }
                if(imageupload1.getDrawable()==null)  {

                    getData1(Id,amount,description);
                }

                else {
                    getData(Id,amount,description);
                }



            }
        });



        upload1=findViewById(R.id.upload);
        upload1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "content://media/internal/images/media"));
                startActivity(intent);*/


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }

        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload1);

            bitmap = ((BitmapDrawable) imageupload1.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);



        } else {

        /*   imageupload1.setImageResource(R.drawable.kb);
            //imageupload1.setVisibility(View.GONE);
            bitmap = ((BitmapDrawable) imageupload1.getDrawable().getCurrent()).getBitmap();
            converetdImage = getResizedBitmap(bitmap, 500);
*/

        }

    }

    private void getData1(String id, final String amount, String description) {





        final ProgressDialog progressDialog=new ProgressDialog(WalletRequest.this);
        progressDialog.setMessage("Verifying Details.....");
        progressDialog.show();


      /*  RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), getFileDataFromDrawable((converetdImage)));
        RequestBody Amount = RequestBody.create(MediaType.parse("multipart/form-data"), amount);
        RequestBody IId = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody Desc = RequestBody.create(MediaType.parse("multipart/form-data"), description);
*/

        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse12> call=apiInterface.Statuss00(Id,amount,description);
        call.enqueue(new Callback<StatusResponse12>() {
            @Override
            public void onResponse(Call<StatusResponse12> call, Response<StatusResponse12> response) {
                if (response.isSuccessful()) ;

                StatusResponse12 statusResponse = response.body();
                if(statusResponse!=null) {

                    StatusDataResponse12 statusDataResponse = statusResponse.status;

                    if (statusDataResponse.code == 200) {
                        progressDialog.dismiss();
//                    Data8 data=statusResponse.data;

                        // String name=data.firstName;
                        //Toast.makeText(WalletRequest.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                        final Dialog dialog = new Dialog(WalletRequest.this);
                        dialog.setContentView(R.layout.dialogbox5);
                        dialog.show();

                        TextView walletrs, walletrs1;
                        walletrs = dialog.findViewById(R.id.walletrs);
                        walletrs1 = dialog.findViewById(R.id.walletrs1);
                        walletrs1.setText(amount);
                        walletrs.setText(amount);

                /*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*/
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Window window = dialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        Button ok = (Button) dialog.findViewById(R.id.ok);
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                           /* Intent intent=new Intent(WalletRequest.this, WalletTransactions1.class);
                            startActivity(intent);
*/
                                finish();
                                // dialog.dismiss();
                            }
                        });
                    } else if (statusDataResponse.code == 409) {
                        progressDialog.dismiss();
                        Toast.makeText(WalletRequest.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(WalletRequest.this, "Error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StatusResponse12> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(WalletRequest.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                file = new File(picturePath);
            }

//            file = new File(picturePath);

//// add another part within the multipart request
//            RequestBody fullName =
//                    RequestBody.create(MediaType.parse("multipart/form-data"), "Your Name");






            try {
                Bitmap  bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload1.setImageBitmap(converetdImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getData(String id, final String amount, String description) {


        final ProgressDialog progressDialog=new ProgressDialog(WalletRequest.this);
        progressDialog.setMessage("Verifying Details.....");
        progressDialog.show();


//        RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), getFileDataFromDrawable((converetdImage)));

//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
//

        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("attachments", file.getName(), requestFile);

        }


        RequestBody Amount = RequestBody.create(MediaType.parse("multipart/form-data"), amount);
        RequestBody IId = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody Desc = RequestBody.create(MediaType.parse("multipart/form-data"), description);


        apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<StatusResponse8> call=apiInterface.Statuss8(body,IId,Amount,Desc);
        call.enqueue(new Callback<StatusResponse8>() {
            @Override
            public void onResponse(Call<StatusResponse8> call, Response<StatusResponse8> response) {
                if (response.isSuccessful()) ;

                StatusResponse8 statusResponse = response.body();
                StatusDataResponse8 statusDataResponse=statusResponse.status;

                if (statusDataResponse.code == 200){
                    progressDialog.dismiss();
//                    Data8 data=statusResponse.data;

                    // String name=data.firstName;
                    //  Toast.makeText(WalletRequest.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                    final Dialog dialog = new Dialog(WalletRequest.this);
                    dialog.setContentView(R.layout.dialogbox);
                    dialog.show();

                    TextView walletrs,walletrs1;
                    walletrs=dialog.findViewById(R.id.walletrs);
                    walletrs1=dialog.findViewById(R.id.walletrs1);
                    walletrs1.setText(amount);
                    walletrs.setText(amount);

                /*InsetDrawable inset = new InsetDrawable(,20);
                dialog.getWindow().setBackgroundDrawable(inset);*/
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    Button ok=(Button)dialog.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           /* Intent intent=new Intent(WalletRequest.this, WalletTransactions1.class);
                            startActivity(intent);
*/
                            finish();
                            // dialog.dismiss();
                        }
                    });
                }
                else if (statusDataResponse.code == 409 ){
                    progressDialog.dismiss();
                    Toast.makeText(WalletRequest.this, statusDataResponse.message, Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<StatusResponse8> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(WalletRequest.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Log.e(TAG, "onResponse: "+t.getMessage());


            }
        });


    }



}
